﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ViaClassLibrary;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading.Tasks;
using System.Threading;
using System.Data;
using System.Drawing;

namespace ViaService
{

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ViaService" in both code and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Multiple)]
    [GlobalErrorHandlerBehaviour(typeof(GlobalErrorHandler))]
    public class ViaService : IViaService
    {
        //public static Dictionary<int, IViaServiceCallBack> usersList = new Dictionary<int, IViaServiceCallBack>();
       // public static Dictionary<int, LoginStatus> usersStatusList = new Dictionary<int, LoginStatus>();
       // public static object usersListLock = new object();
       // public static object usersStatusListLock = new object();


        private object _statusCheckLock = new object();
        private ReaderWriterLockSlim subscribersLock = new ReaderWriterLockSlim();

        private LoginStatus _loginStatus = LoginStatus.Offline;
        private IViaServiceCallBack _calBack;
        private CancellationTokenSource _ctsOnlineThread;
        private CancellationToken _ctOnlineThread;
        private Task _tCheckStatus;
        private ViaUser _user;
        List<ViaFriend> _fList = new List<ViaFriend>();
        private object _fListLock = new object();
        private Task _initialTask;
        //private Task _initialVideoTask;
        private Task _contactRefreshTask;
        private Task _syncFriendStatus;



        public LoginStatus ClientLoginStatus
        {
            get
            {
                return this._loginStatus;
            }
            set
            {
                this._loginStatus = value;
                Task modeChangeTask = Task.Run(() => this.ChangeMode(value));
            }
        }

        public List<ViaFriend> FList
        {
            get
            {
                return this._fList;
            }
        }

        //authenticate user and send back user object
        public ViaUser login(string email, string pass)
        {
            ViaUser user = new ViaUser();
            DataBase db = new DataBase();
            ViaImage img = new ViaImage();
            SqlDataReader data = db.GetUserDetails(email, pass);
            if (data.Read())
            {
                user.ID = Convert.ToInt32(data["u_id"]);
                user.Emial = data["email"].ToString();
                user.FirstName = data["f_name"].ToString();
                user.LastName = data["l_name"].ToString();
                user.Image = img.GetImage(ConfigurationManager.AppSettings["ImagePath"], data["u_image"].ToString(), new System.Drawing.Size(100, 100));
                //this.ClientLoginStatus = LoginStatus.Online;
                data.Close();
                this._user = user;
                IViaServiceCallBack callBack = OperationContext.Current.GetCallbackChannel<IViaServiceCallBack>();
                this._initialTask = Task.Run(() => Initial(callBack));
                //Task newT = Task.Run(() => continuee());
                return user;
            }
            else
            {
                return user = null;
            }

        }


        //set user mode to online
        public bool Connect()
        {
            this.ClientLoginStatus = LoginStatus.Online;
            return true;
        }


        private void CheckClientStatus()
        {
            try
            {
                while (true)
                {
                    Thread.Sleep(1000);
                    if (_ctOnlineThread.IsCancellationRequested)
                    {
                        break;
                    }
                    this._calBack.CheckClientStatus();
                }
            }
            catch (CommunicationException)
            {
                if (this.ClientLoginStatus != LoginStatus.Offline)
                {
                    this.ClientLoginStatus = LoginStatus.Away;
                }
            }
            catch (TimeoutException)
            {
                if (this.ClientLoginStatus != LoginStatus.Offline)
                {
                    this.ClientLoginStatus = LoginStatus.Away;
                }
            }
        }


        public void LogOut()
        {
            this.ClientLoginStatus = LoginStatus.Offline;
        }


        //Change client mode
        //every process change according to user state
        private void ChangeMode(LoginStatus status)
        {
            //execute this code when ClientStatus changed to Away
            if (status.Equals(LoginStatus.Away))
            {
                //Console.WriteLine(_user.FirstName + " : is Away.....");
                //lock (usersList)
                //    if (usersList.ContainsKey(_user.ID))
                //    {
                //        usersList.Remove(_user.ID);
                //    }
                ViaConnector.RemoveStatus(_user.ID);
                SetUserDetails(LoginStatus.Away);
                this.SendStatus(LoginStatus.Away);
                this._ctsOnlineThread.Cancel();

            }

            //execute this code when ClientStatus changed to Offline
            if (status.Equals(LoginStatus.Offline))
            {
               // Console.WriteLine(_user.FirstName + " : is Offline.....");
                this._ctsOnlineThread.Cancel();
                ViaConnector.RemoveStatus(_user.ID);
                ViaConnector.RemoveCallBack(_user.ID);
                //lock (usersStatusListLock)
                //{
                //    if (usersStatusList.ContainsKey(_user.ID))
                //        usersStatusList.Remove(_user.ID);
                //}
                //lock (usersListLock)
                //{
                //    if (usersList.ContainsKey(_user.ID))
                //        usersList.Remove(_user.ID);
                //}
                this.SendStatus(LoginStatus.Offline);
            }

            //execute this code when ClientStatus changed to Online
            if (status.Equals(LoginStatus.Online))
            {
               // Console.WriteLine(_user.FirstName + " : is Online.....");
                this._initialTask.Wait();
                InitialContacts();
                lock (_fList)
                {
                    this._calBack.SyncContacts(this._fList);
                }
                //lock (usersListLock)
                //{
                //    if (usersList.ContainsKey(_user.ID))
                //    {
                //        usersList[_user.ID] = this._calBack;
                //    }
                //    else
                //    {
                //        usersList.Add(_user.ID, this._calBack);
                //    }
                //}
                ViaConnector.AddUser(_user.ID, this._calBack);
                SetUserDetails(LoginStatus.Online);
                this._tCheckStatus = Task.Run(() => this.CheckClientStatus(), _ctOnlineThread);
                Task.Run(() => SyncFriendStatus());
                this.SendStatus(LoginStatus.Online);
                MessageSyncRequest();
            }
        }

        //set user call back to userList
        //set user status to userStatusList
        private void SetUserDetails(LoginStatus status)
        {
            //lock (usersStatusListLock)
            //{
            //    if (usersStatusList.ContainsKey(_user.ID))
            //    {
            //        usersStatusList[_user.ID] = status;
            //    }
            //    else
            //    {
            //        usersStatusList.Add(_user.ID, status);
            //    }
            //}
            ViaConnector.SetUserStatus(_user.ID, status);
        }

        private void Initial(IViaServiceCallBack callBack)
        {
            this._calBack = callBack;
            this._ctsOnlineThread = new CancellationTokenSource();
            this._ctOnlineThread = this._ctsOnlineThread.Token;

        }

        private void SyncFriendStatus()
        {
            if (_contactRefreshTask != null)
            {
                _contactRefreshTask.Wait();
            }
            lock (_fListLock)
            {
                foreach (ViaFriend friend in _fList)
                {
                    //lock (usersStatusListLock)
                    //{
                    //    if (usersStatusList.ContainsKey(friend.ID))
                    //    {
                    //        this._calBack.SyncFriendsStatus(friend.ID, usersStatusList[friend.ID]);
                    //    }
                    //    else
                    //    {
                    //        this._calBack.SyncFriendsStatus(friend.ID, LoginStatus.Offline);
                    //    }
                    //}
                    this._calBack.SyncFriendsStatus(friend.ID, ViaConnector.GetStatusById(friend.ID));
                }
            }
        }

        public List<ViaFriend> GetFriendList()
        {
            return _fList;
        }

        private void InitialContacts()
        {
            DataBase db = new DataBase();
            ViaImage img = new ViaImage();
            Size size = new Size(100, 100);
            DataSet ds = db.GetFriendList(this._user.ID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    byte[] image = img.GetImage(@"E:\image", row["u_image"].ToString(), size);
                    ViaFriend friend = new ViaFriend(Convert.ToInt32(row["u_id"]), row["f_name"].ToString(), row["l_name"].ToString(), row["email"].ToString(), image, Convert.ToInt32(row["requester"]), Convert.ToInt32(row["accepter"]), Convert.ToDateTime(row["request_date"]), Convert.ToInt32(row["f_status"]));
                    lock (_fListLock)
                    {
                        if (!_fList.Contains(friend))
                        {
                            _fList.Add(friend);
                        }
                    }
                }
            }
        }

        private void SendStatus(LoginStatus status)
        {
            //Broadcast bc = new Broadcast(_user.ID);
            lock (_fListLock)
            {
                foreach (ViaFriend friend in _fList)
                {
                   // bc.SendStatus(friend.ID, status);
                    ViaConnector.SendStatus(friend.ID, _user.ID, status);
                }
            }
        }


        public MessageArgs SendMsg(ViaMessage msg)
        {
            Task t = Task.Run(() => SendMsgHandle(msg));
            return MessageArgs.Send;
        }
        private void SendMsgHandle(ViaMessage msg)
        {
            int count = 1;
            MessageArgs args = MessageArgs.SendingFail;
            DataBase db = new DataBase();
            //Broadcast bc = new Broadcast(_user.ID);
            while (count <= 3)
            {
                count++;
                //args = bc.SendMessage(msg);
                args = ViaConnector.SendMsg(msg);
                if (args.Equals(MessageArgs.Send))
                {
                    break;
                }
                else
                {
                    Task.Delay(1000);
                    continue;
                }
            }
            if (args.Equals(MessageArgs.SendingFail))
            {
                db.SaveMessage(msg.Sender, msg.Receiver, msg.Date, msg.Message);
            }
        }


        public void MessageSyncRequest()
        {
            DataBase db = new DataBase();
            DataSet ds = db.GetMessages(_user.ID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                   // Console.WriteLine("Message received from " + row["sender"].ToString() + "  to " + row["receiver"]);
                    ViaMessage msg = new ViaMessage(Convert.ToInt32(row["sender"]), Convert.ToInt32(row["receiver"]), row["m_content"].ToString(), Convert.ToDateTime(row["m_datetime"]));
                    Task.Run(() => MessageSync(msg, Convert.ToInt32(row["msg_id"])));
                }
            }
        }

        private void MessageSync(ViaMessage msg, int msgId)
        {
            MessageArgs res = _calBack.MessageReceived(msg);
            if (res.Equals(MessageArgs.Received))
            {
              //  Console.WriteLine("Message id : " + msgId + " received : " + res.ToString());
                DataBase db = new DataBase();
                db.DelMsg(msgId);
            }
        }


        public List<ViaUser> Search(string searchKey)
        {
            DataBase db = new DataBase();
            ViaImage img = new ViaImage();
            Size size = new Size(100, 100);
            List<ViaUser> resList = new List<ViaUser>();
            DataSet ds = db.Search(searchKey);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    lock (_fListLock)
                    {
                        if (!_fList.Exists(x => x.ID == Convert.ToInt32(row["u_id"])))
                        {
                            if (_user.ID == Convert.ToInt32(row["u_id"]))
                            {
                                continue;
                            }
                            byte[] image = img.GetImage(@"E:\image", row["u_image"].ToString(), size);
                            resList.Add(new ViaUser(Convert.ToInt32(row["u_id"]), row["f_name"].ToString(), row["l_name"].ToString(), row["email"].ToString(), image));
                        }
                    }
                }
            }
            return resList;
        }


        public bool AddContact(int accepterId)
        {
            DataBase db = new DataBase();
            if (!db.CheckFriend(accepterId, _user.ID))
            {
               // Console.WriteLine("Accepter :" + accepterId + " requesterID :" + _user.ID);
                db.SaveFriend(accepterId, _user.ID, DateTime.Now, 1);
                Task t = Task.Run(() => SendContact(accepterId));
                return true;
            }
            else
            {
                return false;
            }
        }

        private void SendContact(int accepter)
        {
            Thread.Sleep(50);
            DataBase db = new DataBase();
            DataSet ds = db.GetFriend(_user.ID, accepter);
            ViaFriend friend = new ViaFriend();
            Size size = new Size(100, 100);
            ViaImage img = new ViaImage();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                byte[] image = img.GetImage(@"E:\image", row["u_image"].ToString(), size);
                friend = new ViaFriend(Convert.ToInt32(row["u_id"]), row["f_name"].ToString(), row["l_name"].ToString(), row["email"].ToString(), image, Convert.ToInt32(row["requester"]), Convert.ToInt32(row["accepter"]), Convert.ToDateTime(row["request_date"]), Convert.ToInt32(row["f_status"]));
            }
            //Broadcast bc = new Broadcast(_user.ID);
            ViaConnector.SendContact(friend, accepter);
            //bc.SendContact(friend, accepter);
        }


        public void FriendListRefreshRequest()
        {
            if (_initialTask != null)
            {
                _initialTask.Wait();
            }
            _contactRefreshTask = Task.Run(() => InitialContacts());
            _syncFriendStatus = Task.Run(() => SyncFriendStatus());
        }

        // Video Calling
        public void GetCall(int id, callType callType)
        {
            try
            {
                IViaServiceCallBack user = null;
                subscribersLock.EnterReadLock();
                user = ViaConnector.getVideoCall(id);
                if (callType == callType.call)
                {
                    user.OnDial(id, callType);
                    //Task.Run(() => user.OnDial(id, callType));
                    //Thread t = new Thread(new ThreadStart(() => user.OnDial(id, callType)));
                    //t.Start();
                }
                else if (callType == callType.cut)
                {
                    user.OnDial(id, callType);
                }
                else if (callType == callType.incomeAnswer)
                {
                    user.OnVideo(id, callType);
                }
                else if (callType == callType.incomeCut)
                {
                    user.OnVideo(id, callType);
                }
                else
                {
                    Console.WriteLine("Error in Client Application");
                }
            }
            finally
            {
                subscribersLock.ExitReadLock();
            }
        }

        public void PublishVideo(int id, byte[] data)
        {
            if (_initialTask != null)
            {
                _initialTask.Wait();
            }

            try
            {
                IViaServiceCallBack user = null;
                //subscribersLock.EnterReadLock();
                user = ViaConnector.publishVideoCall(id);
                //user.OnVideoSend(id, data);
                Thread tt = new Thread(new ThreadStart(() => user.OnVideoSend(id, data)));
                tt.Start();
                //Console.WriteLine(id);
            }
            finally
            {
                //subscribersLock.ExitReadLock();
            }

            //try
            //{
            //    subscribersLock.EnterReadLock();
            //    this._calBack.OnVideoSend(id, data);
            //}
            //finally
            //{
            //    subscribersLock.ExitReadLock();
            //}
        }

        public void HoldVideo(int id, holdType holdType)
        {
            try
            {
                IViaServiceCallBack user = null;
                subscribersLock.EnterReadLock();
                user = ViaConnector.holdVideoCall(id);
                if (holdType == holdType.hold)
                {
                    user.OnHold(id, holdType);
                }
                else if (holdType == holdType.unhold)
                {
                    user.OnHold(id, holdType);
                }
                else
                {
                    Console.WriteLine("Error hold video in Client Application");
                }
            }
            finally
            {
                subscribersLock.ExitReadLock();
            }
        }

        public void CutCall(int id)
        {
            try
            {
                IViaServiceCallBack user = null;
                subscribersLock.EnterReadLock();
                user = ViaConnector.CutCall(id);
                Thread ttt = new Thread(new ThreadStart(() => user.OnCut(id)));
                ttt.Start();
                //user.OnCut(id);
            }
            finally
            {
                subscribersLock.ExitReadLock();
            }
        }
    }
}
