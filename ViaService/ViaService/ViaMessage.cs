﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ViaService
{
    [DataContract]
    public class ViaMessage
    {
        private int _sender;
        private int _receiver;
        private string _msg;
        private DateTime _date;

     
        [DataMember]
        public int Sender
        {
            get
            {
                return this._sender;
            }
            set {
                this._sender = value;
                }
        }

        [DataMember]
        public int Receiver 
        {
            get
            {
                return this._receiver;
            }
            set
            {
                this._receiver = value;
            }
        }
        [DataMember]
        public string Message
        {
            get
            {
                return this._msg;
            }
            set
            {
                this._msg = value;
            }
        }
      
        [DataMember]
        public DateTime Date
        {
            get
            {
                return this._date;
            }
            set
            {
                this._date = value;
            }
        }

        
        public ViaMessage() { }

        
        public ViaMessage(int sender, int receiver, string msg, DateTime date)
        {
            this._sender = sender;
            this._receiver = receiver;
            this._msg = msg;
            this._date = date;
        }
    }
}
