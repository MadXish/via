﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ViaService
{
    [ServiceContract(CallbackContract = typeof(IViaServiceCallBack))]
    public interface IViaService
    {
        //return user if request authenticate
        [OperationContract]
        ViaUser login(string email, string pass);

        //logout request one way request
        [OperationContract(IsOneWay = true)]
        void LogOut();

        [OperationContract]
        List<ViaFriend> GetFriendList();

        [OperationContract]
        bool Connect();

        [OperationContract]
        MessageArgs SendMsg(ViaMessage msg);

        [OperationContract]
        List<ViaUser> Search(string searchKey);

        [OperationContract]
        bool AddContact(int accepterId);

        [OperationContract(IsOneWay=true)]
        void FriendListRefreshRequest();

        [OperationContract]
        void GetCall(int id, callType callType);

        [OperationContract]
        void HoldVideo(int id, holdType holdType);

        [OperationContract]
        void PublishVideo(int id, byte[] data);

        [OperationContract]
        void CutCall(int id);
    }

    [ServiceContract]
    public interface IViaWebService
    {
        [OperationContract]
        ViaUser login(string email, string pass);

        [OperationContract]
        bool Register(string email, string pass,string fName,string lName,string location,DateTime dob,string tel,byte[] image);
    }


    //CallBack Interface
    public interface IViaServiceCallBack
    {
        [OperationContract]
        MessageArgs MessageReceived(ViaMessage msg);

        [OperationContract]
        void SyncFriendsStatus(int userId, LoginStatus status);

        [OperationContract]
        void SyncContacts(List<ViaFriend> friend);

        [OperationContract]
        void CheckClientStatus();

        [OperationContract]
        void OnDial(int id, callType callType);

        [OperationContract]
        void OnVideo(int id, callType callType);

        [OperationContract]
        void OnHold(int id, holdType holdType);

        [OperationContract]
        void OnCut(int id);

        [OperationContract]
        void OnVideoSend(int id, byte[] data);
    }
}
