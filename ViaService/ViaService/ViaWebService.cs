﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using ViaClassLibrary;
using System.Runtime.Serialization;

namespace ViaService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Single)]
    public class ViaWebService : IViaWebService
    {
        private ViaUser _user;

        public ViaUser User
        {
            get
            {
                return this._user;
            }
            set
            {
                this._user = value;
            }
        }

        public ViaUser login(string email, string pass)
        {
            Console.WriteLine(email.ToString() + "" + pass.ToString());
            ViaUser user = new ViaUser();
            DataBase db = new DataBase();
            ViaImage img = new ViaImage();
            SqlDataReader data = db.GetUserDetails(email, pass);
            if (data.Read())
            {
                user.ID = Convert.ToInt32(data["u_id"]);
                user.Emial = data["email"].ToString();
                user.FirstName = data["f_name"].ToString();
                user.LastName = data["l_name"].ToString();
                user.Image = img.GetImage(ConfigurationManager.AppSettings["ImagePath"], data["u_image"].ToString(), new System.Drawing.Size(100, 100));
                //this.ClientLoginStatus = LoginStatus.Online;
                data.Close();
                this._user = user;
                return user;
            }
            else
            {
                return user = null;
            }
        }


        public bool Register(string email, string pass, string fName, string lName, string location, DateTime dob, string tel, byte[] image)
        {
            Random rNo = new Random();
            string r = rNo.Next(1000).ToString();
            string imageName = fName + r + ".jpg";
            DataBase db = new DataBase();
            ViaImage img = new ViaImage();
            img.SaveImage(image, imageName, ConfigurationManager.AppSettings["ImagePath"]);
            bool res = db.RegisterUser(email, pass, fName, lName, location, dob, tel, imageName);
            if (res)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
