﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViaService
{
    public enum CallBackArgs
    {
        Message,
        FriendRequest,
        Notification,
        LoginStatus,
        Success,
        Unsuccess
    }

    public enum MessageArgs
    {
        Send = 1,
        Received = 2,
        SendingFail = 0,
        ReceivingFail = 3
    }

    public enum LoginStatus
    {
        Online = 0,
        Away = 1,
        Invisible = 2,
        Offline = 3
    }

    public enum callType
    {
        call = 1,
        cut = 2,
        incomeAnswer = 3,
        incomeCut = 4
    }

    public enum holdType
    {
        hold = 1,
        unhold = 2
    }
}
