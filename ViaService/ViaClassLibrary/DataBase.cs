﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace ViaClassLibrary
{
    public class DataBase
    {
        private string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        public DataBase()
        {
        }
        public DataBase(string conString)
        {
            cs = conString;
        }

        public SqlDataReader GetUserDetails(string email, string pass)
        {
            SqlDataReader data;
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spGetUser", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramEmail = new SqlParameter("@email", email);
            cmd.Parameters.Add(pramEmail);
            SqlParameter pramPass = new SqlParameter("@pass", pass);
            cmd.Parameters.Add(pramPass);
            cn.Open();
            data = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return data;
        }

        public DataSet GetFriendList(int userId)
        {
            SqlDataAdapter adt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spGetFriends", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramId = new SqlParameter("@id", userId);
            cmd.Parameters.Add(pramId);
            cn.Open();
            adt.SelectCommand = cmd;
            adt.Fill(ds);
            cn.Close();
            return ds;
        }

        public DataSet GetMessages(int userId)
        {
            SqlDataAdapter adt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spGetMessages", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramId = new SqlParameter("@receiver", userId);
            cmd.Parameters.Add(pramId);
            cn.Open();
            adt.SelectCommand = cmd;
            adt.Fill(ds);
            cn.Close();
            return ds;
        }

        public int SaveMessage(int sender, int receiver, DateTime date,string msg)
        {
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spSaveMsg", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramSender = new SqlParameter("@sender", sender);
            cmd.Parameters.Add(pramSender);
            SqlParameter pramReceiver = new SqlParameter("@receiver", receiver);
            cmd.Parameters.Add(pramReceiver );
            SqlParameter pramContent = new SqlParameter("@mContent", msg);
            cmd.Parameters.Add(pramContent);
            SqlParameter pramDate = new SqlParameter("@mDate", date);
            cmd.Parameters.Add(pramDate);
            cn.Open();
            int id = (int)cmd.ExecuteScalar();
            cn.Close();
            return id;
            
        }

        public void DelMsg(int msgId)
        {
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spDeleteMsg", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramSender = new SqlParameter("@msgId",msgId);
            cmd.Parameters.Add(pramSender);
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public DataSet Search(string searchKey)
        {
            SqlDataAdapter adt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spSearch", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramId = new SqlParameter("@SearchKey", searchKey);
            cmd.Parameters.Add(pramId);
            cn.Open();
            adt.SelectCommand = cmd;
            adt.Fill(ds);
            cn.Close();
            return ds;
        }

        public bool CheckFriend(int accepter, int requester)
        {
            SqlDataAdapter adt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spCheckFriend", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramAccepter = new SqlParameter("@accepter", accepter);
            cmd.Parameters.Add(pramAccepter);
            SqlParameter pramRequester = new SqlParameter("@requester", requester);
            cmd.Parameters.Add(pramRequester);
            cn.Open();
            adt.SelectCommand = cmd;
            adt.Fill(ds);
            cn.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SaveFriend(int accepter, int requester, DateTime date, int status)
        {
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spSaveFriend", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramSender = new SqlParameter("@accepter", accepter);
            cmd.Parameters.Add(pramSender);
            SqlParameter pramReceiver = new SqlParameter("@requester", requester);
            cmd.Parameters.Add(pramReceiver);
            SqlParameter pramDate = new SqlParameter("@request_date", date);
            cmd.Parameters.Add(pramDate);
            SqlParameter pramStatus = new SqlParameter("@f_status", status);
            cmd.Parameters.Add(pramStatus);
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public DataSet GetFriend(int requester,int accepter)
        {
            SqlDataAdapter adt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spGetFriend", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramRequester = new SqlParameter("@requester", requester);
            cmd.Parameters.Add(pramRequester);
            SqlParameter pramAccepter = new SqlParameter("@accepter", accepter);
            cmd.Parameters.Add(pramAccepter);
            cn.Open();
            adt.SelectCommand = cmd;
            adt.Fill(ds);
            cn.Close();
            return ds;
        }

        public DataSet GetMsg(int msgId)
        {
            SqlDataAdapter adt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spGetMsg", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramAccepter = new SqlParameter("@msgId", msgId);
            cmd.Parameters.Add(pramAccepter);
            cn.Open();
            adt.SelectCommand = cmd;
            adt.Fill(ds);
            cn.Close();
            return ds;
        }

        public bool RegisterUser(string email, string pass, string fName, string lName, string location, DateTime dob, string tel,string image)
        {
            DateTime joinDate = DateTime.Now;
            //try
            //{
                SqlConnection cn = new SqlConnection(cs);
                SqlCommand cmd = new SqlCommand("spSaveUser", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter pramEmail = new SqlParameter("@email", email);
                cmd.Parameters.Add(pramEmail);
                SqlParameter pramPass = new SqlParameter("@pass", pass);
                cmd.Parameters.Add(pramPass);
                SqlParameter pramFName = new SqlParameter("@f_name", fName);
                cmd.Parameters.Add(pramFName);
                SqlParameter pramLName = new SqlParameter("@l_name", lName);
                cmd.Parameters.Add(pramLName);
                SqlParameter pramLocation = new SqlParameter("@location", location);
                cmd.Parameters.Add(pramLocation);
                SqlParameter pramDob = new SqlParameter("@dob", dob);
                cmd.Parameters.Add(pramDob);
                SqlParameter pramImage = new SqlParameter("@image", image);
                cmd.Parameters.Add(pramImage);
                SqlParameter paraJoin = new SqlParameter("@join_date", joinDate);
                cmd.Parameters.Add(paraJoin);
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
                return true;
            //}
            //catch (Exception)
            //{
            //    return false;
            //}
        }
    }
}
