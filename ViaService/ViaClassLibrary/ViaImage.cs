﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViaClassLibrary
{
    public class ViaImage
    {
        private ImageFormat imageFormat = ImageFormat.Jpeg;
        public byte[] ImageToByteArray(Image image)
        {
            if (image.RawFormat.Equals(ImageFormat.Jpeg))
                imageFormat = ImageFormat.Jpeg;
            if (image.RawFormat.Equals(ImageFormat.Gif))
                imageFormat = ImageFormat.Gif;
            if (image.RawFormat.Equals(ImageFormat.Png))
                imageFormat = ImageFormat.Png;

            MemoryStream ms = new MemoryStream();
            image.Save(ms, imageFormat);
            return ms.ToArray();
        }

        public Image ByteArrayToImage(byte[] byteArray)
        {
            MemoryStream ms = new MemoryStream(byteArray);
            return Image.FromStream(ms);

        }

        public Image ResizeImage(Image image, Size size)
        {
            return (Image)(new Bitmap(image, size));
        }

        public  byte[] GetImage(string path, string name, Size size)
        {
           return ImageToByteArray((Image)(new Bitmap(ByteArrayToImage( File.ReadAllBytes(path + "\\" + name)),size)));
        }

        public void SaveImage(byte[] image, string imageName,string path)
        {
            System.IO.File.WriteAllBytes(path + "\\" + imageName, image);
        }
    }
}
