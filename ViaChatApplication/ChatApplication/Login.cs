﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using System.Diagnostics;

namespace ChatApplication
{
    public partial class Login : Form
    {
        Program main;
        public Login(Program fHandler)
        {
           this.main = fHandler;
           InitializeComponent();
        }
        
        private async void UserLogin(object sender, EventArgs e)
        {
            int atempt = 0;
            lblLoginStatus.Text = "";
            while (true)
            {
                atempt++;
                if (atempt == 10)
                {
                    lblLoginStatus.Text = "Couldn't Connect to the Via Service...";
                    break;
                }
                pnlLogin.Hide();
                pnlWaiting.Show();
                if (main.Client.State == CommunicationState.Faulted)
                    main.Client = new ViaService.ViaServiceClient(main.MainIns);
                try
                {
                    Hash h = new Hash();
                    ViaService.ViaUser user = await main.Client.loginAsync(txtEmail.Text, h.GenerateSHA256Hash(txtPass.Text));
                    if (user != null)
                    {
                        main.User = user;
                        main.FormArgument = FormReturngArguments.Login;
                        this.Close();
                    }
                    else
                    {
                        lblLoginStatus.Text = "Sorry... We didn't recognise your Sign-in details.Please check your Email and Password, then try again...";
                        break;
                    }
                }
                catch (Exception)
                {
                    continue;
                }
                
            }
            pnlLogin.Show();
            pnlWaiting.Hide();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            this.Initial();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("www.qdrains.com");
        }

        private void Initial()
        {
            lblLoginStatus.Text = "";
            txtEmail.Text = "isharazone@gmail.com";
            txtPass.Text = "123456";
            pnlWaiting.Hide();
            pnlLogin.Show();
        }

        private void ovalShape1_MouseEnter(object sender, EventArgs e)
        {
            label5.ForeColor = Color.White;
            ovalShape2.FillColor = Color.DeepSkyBlue;
            ovalShape1.FillColor = Color.DeepSkyBlue;
            panel1.BackColor = Color.DeepSkyBlue;
        }

        private void ovalShape2_MouseEnter(object sender, EventArgs e)
        {
            label5.ForeColor = Color.White;
            ovalShape2.FillColor = Color.DeepSkyBlue;
            ovalShape1.FillColor = Color.DeepSkyBlue;
            panel1.BackColor = Color.DeepSkyBlue;
        }

        private void panel1_MouseEnter(object sender, EventArgs e)
        {
            label5.ForeColor = Color.White;
            ovalShape2.FillColor = Color.DeepSkyBlue;
            ovalShape1.FillColor = Color.DeepSkyBlue;
            panel1.BackColor = Color.DeepSkyBlue;
        }

        private void label5_MouseEnter(object sender, EventArgs e)
        {
            label5.ForeColor = Color.White;
            ovalShape2.FillColor = Color.DeepSkyBlue;
            ovalShape1.FillColor = Color.DeepSkyBlue;
            panel1.BackColor = Color.DeepSkyBlue;
        }

        private void panel1_MouseLeave(object sender, EventArgs e)
        {
            label5.ForeColor = Color.White;
            ovalShape2.FillColor = Color.DodgerBlue;
            ovalShape1.FillColor = Color.DodgerBlue;
            panel1.BackColor = Color.DodgerBlue;
        }

        private void ovalShape1_MouseLeave(object sender, EventArgs e)
        {
            label5.ForeColor = Color.White;
            ovalShape2.FillColor = Color.DodgerBlue;
            ovalShape1.FillColor = Color.DodgerBlue;
            panel1.BackColor = Color.DodgerBlue;
        }

        private void label5_MouseLeave(object sender, EventArgs e)
        {
            label5.ForeColor = Color.White;
            ovalShape2.FillColor = Color.DodgerBlue;
            ovalShape1.FillColor = Color.DodgerBlue;
            panel1.BackColor = Color.DodgerBlue;
        }

        private void ovalShape2_MouseLeave(object sender, EventArgs e)
        {
            label5.ForeColor = Color.White;
            ovalShape2.FillColor = Color.DodgerBlue;
            ovalShape1.FillColor = Color.DodgerBlue;
            panel1.BackColor = Color.DodgerBlue;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            main.FormArgument = FormReturngArguments.Close;
            this.Close();
        }

    }
}
