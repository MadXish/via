﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using ChatApplication.ViaService;
using System.Threading;

namespace ChatApplication
{
    [CallbackBehavior(UseSynchronizationContext = false)]

    public class Program 
    {

        private ViaServiceClient _client;
        private ViaUser _user;
        private FormReturngArguments? _formArgument = null;
        private LoginStatus _loginStatus = LoginStatus.Offline;
        private Login _login;
        private Main _main;
        private InstanceContext _mainIns;
        
        

        public LoginStatus Loginstatus
        { 
            get {
                return this._loginStatus;
            }
            set {
                this._loginStatus = value;
            }
        }
        public ViaServiceClient Client
        {
            get
            {
                return this._client;

            }
            set
            {
                this._client = value;
            }
        }
        public ViaUser User
        {
            get
            {
                return this._user;
            }
            set
            {
                this._user = value;
            }
        }
        public FormReturngArguments? FormArgument
        {
            get
            {
                return this._formArgument;
            }
            set
            {

                this._formArgument = value;
            }
        }

        public InstanceContext MainIns
        {
            get
            {
                return this._mainIns;
            }
        }


        

        [STAThread]
        static void Main()
        {
          
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Program p = new Program();

                p.Initial();
                p.Login();
            

        }

        public void SetUser(ViaUser u)
        {
            
            this._user = u;
        }

        public void Login()
        {
            Application.Run(_login);
            if (this._formArgument.Equals(FormReturngArguments.Login))
            {
                this.MainForm();
            }
            else
            {
                System.Environment.Exit(Environment.ExitCode);
            }
        }

        public void MainForm()
        {
            Application.Run(_main);
            if (this._formArgument.Equals(FormReturngArguments.SingOut))
            {
                this.Initial();
                this.Login();
            }

        }

        private void Initial()
        {
            this.User = null;
            this.FormArgument = null;
            this._login = new Login(this);
            this._main = null;
            this._main = new Main(this);
            _mainIns = new InstanceContext(_main);
            this._client = new ViaServiceClient(_mainIns);
        }
    }
}
