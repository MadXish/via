﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ChatApplication;
using ChatApplication.ViaService;
using System.Threading;
using Microsoft.VisualBasic.PowerPacks;
using System.Collections;
using System.ServiceModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.IO;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Emgu.CV;
using Emgu.CV.Structure;

namespace ChatApplication
{
    //loosly cupled deligates
    public delegate void InitialHandler();

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public partial class Main : Form, IViaServiceCallback
    {
        private delegate void DrawMControlHandler(ViaMessage msg, MessageArgs type, int msgId);
        private delegate void MessageReceiveHandler(ViaMessage msg, int msgId);
        private delegate void DrawFControlHandler(ViaFriend friend);
        private delegate void DrawSControlHandler(ViaUser user);
        private delegate void SearchHandler(string searchKey);
        private delegate void AddMsgPanelHandler(int p);

        //this form's field members
        private bool _closingFlag = false;
        private LoginStatus _userLoginStatus;
        private Program _fHandler;
        private int _previousePanel = 0;
        private object _floFriendListLock = new object();
        private Task _contactsSyncTask = null;
        private int _selectedContactId;
        private PanelType _selectedPanel;
        private Dictionary<int, Contact> _contactList = new Dictionary<int, Contact>();
        private object _contactListLock = new object();
        private int _connectionTimeOutCount = 0;
        private List<int> _notSeenMsgCount = new List<int>();
        private List<int> _recentList = new List<int>();
        private object _sendMsgLock = new object();
        private CancellationTokenSource _ctsSearch = new CancellationTokenSource();
        private CancellationToken _ctSearch;
        private object _serachLock = new object();
        private Dictionary<int, Contact> _searchContactList = new Dictionary<int, Contact>();
        private Task _searchTask;
        private int _selectedSearchContact = 0;

        //Cam variables
        private Capture _capture = null;
        Image<Bgr, Byte> frame;
        int cam = 0;
        double webcam_frm_cnt = 0;
        double FrameRate = 0;
        double TotalFrames = 0;
        double Framesno = 0;
        double codec_double = 0;


        public LoginStatus UserLoginStatus
        {
            get
            {
                return this._userLoginStatus;
            }
            set
            {
                this._userLoginStatus = value;
                ChangeMode(value);
            }
        }

        public Main(Program formHandler)
        {
            InitializeComponent();
            this._fHandler = formHandler;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            picBoxUser.BackgroundImage = ViaImage.ByteArrayToImage(_fHandler.User.Image);
            lblUserName.Text = _fHandler.User.FirstName + " " + _fHandler.User.LastName;
            Task initialTask = Task.Run(() => this.Initial());
            FormControlInitial(); 
        }

        private void Initial()
        {
            if (floFriendList.InvokeRequired | pnlInitialContact.InvokeRequired | pnlType.InvokeRequired)
            {
                Invoke(new InitialHandler(Initial));
            }
            else
            {

                Database db = new Database();
                //should handel db exception(not save)
                //give error when save friends if this not save
                if (!db.CheckUser(_fHandler.User.ID))
                {
                    //save user to db
                    db.SaveUser(_fHandler.User);
                }
                LoadFriendList();
                if (_contactsSyncTask != null)
                {
                    _contactsSyncTask.Wait();
                }

                Start();
            }

        }

        private void FormControlInitial()
        {
            pnlRecentCount.Hide();
            _notSeenMsgCount.Clear();
            pnlConnecting.Hide();
            pnlContact.BackColor = Color.SkyBlue;
            _selectedPanel = PanelType.Contact;
            floFriendList.Hide();
            flopnlRecent.Hide();
            flopnlSearch.Hide();
            pnlInitialContact.Show();
            pnlType.Enabled = false;
            pnlSearch.BackColor = Color.DeepSkyBlue;
            pnlRecent.BackColor = Color.DeepSkyBlue;
            picSelectedUser.BackgroundImage = null;
            ovlSelectUserStatus.BackgroundImage = null;
            lblSelectUserName.Text = "";
            lblSelectUserStatus.Text = "";
            pnlSearchMessageContainer.Hide();
            pnlMsgWait.Hide();
            panel11.BackColor = SystemColors.Control;

            //Video call panal            
            //lblNo.Text = _fHandler.User.ID.ToString();
            pnlVideoDial.Visible = false;
            defaultPanalSettings();
        }

        public void defaultPanalSettings()
        {
            lblCallStatus.Text = "";
            picAnswerIn.Visible = false;
            picCutIn.Visible = false;
            picCutOut.Visible = false;
            picManualStart.Visible = false;
            picStop.Visible = false;
        }
        private void Start()
        {
            try
            {
                if (this._fHandler.Client.Connect())
                {
                    this.UserLoginStatus = LoginStatus.Online;
                    pnlInitialContact.Hide();
                    floFriendList.Show();
                }
                else
                {
                    this.TryingToConnect();
                }

            }
            catch (Exception)
            {
                TryingToConnect();
            }
        }

        private async void TryingToConnect()
        {
                pnlConnecting.Show();
                timerCheckConnection.Stop();
                if (UserLoginStatus != LoginStatus.Away)
                {
                    this.UserLoginStatus = LoginStatus.Away;
                }
                while (true)
                {
                    try
                    {
                        if (await this._fHandler.Client.ConnectAsync())
                        {
                            this.UserLoginStatus = LoginStatus.Online;
                            pnlConnecting.Hide();
                            break;
                        }
                        Thread.Sleep(100);
                    }
                    catch (CommunicationObjectFaultedException)
                    {
                        ComObjFaultHandle();
                        break;
                    }
                    catch (CommunicationObjectAbortedException)
                    {
                        ComObjFaultHandle();
                        break;
                    }
                    catch (CommunicationException)
                    {
                        continue;
                    }
                }
        }

        private void ComObjFaultHandle()
        {
            MessageBox.Show("Communication refuse by server.\n Please Login again...", "Communication Refuse..!!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            SignOut();
        }

        //after login get contact list(only when start)
        private void LoadFriendList()
        {
            Database db = new Database();
            DataSet ds = db.GetFriendList(_fHandler.User.ID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    ViaFriend friend = new ViaFriend();
                    friend.ID = Convert.ToInt32(row["u_id"]);
                    friend.FirstName = row["f_name"].ToString();
                    friend.LastName = row["l_name"].ToString();
                    friend.Emial = row["email"].ToString();
                    friend.Image = (byte[])row["u_image"];
                    friend.Accepter = Convert.ToInt32(row["accepter"]);
                    friend.Requester = Convert.ToInt32(row["requester"]);
                    friend.Date = Convert.ToDateTime(row["request_date"]);
                    friend.Status = Convert.ToInt32(row["f_status"]);
                    lock (_contactListLock)
                    {
                        Contact contact = new Contact(friend);
                        //event that raise when contact status changed
                        contact.ContactStatusChange += contact_ContactStatusChange;
                        _contactList.Add(friend.ID, contact);
                    }
                    this.DrawFControl(friend);
                }
            }
            else
            {
                try
                {
                    List<ViaFriend> fList = this._fHandler.Client.GetFriendList();
                    this._contactsSyncTask = Task.Run(() => this.SyncContacts(fList));
                }
                catch (Exception)
                {
                    this.TryingToConnect();
                }

            }

        }

        void contact_ContactStatusChange(object sender, ContactStatusChangeEventArgs e)
        {
            Contact contact = (Contact)sender;
            contact.OvlContactStatus.BackgroundImage = imgListStatus.Images[(int)e.LoginStatus];
            if (_selectedPanel != PanelType.Search && _selectedContactId == contact.UserDetails.ID)
            {
                lblSelectUserStatus.Text = e.LoginStatus.ToString();
                ovlSelectUserStatus.BackgroundImage = imgListStatus.Images[(int)e.LoginStatus];
            }
            if (contact.OvlRecentContact != null)
            {
                contact.OvlRecentContact.BackgroundImage = imgListStatus.Images[(int)e.LoginStatus];
            }
        }

        //Draw Friend list
        private void DrawFControl(ViaFriend friend)
        {
            if (floFriendList.InvokeRequired)
            {
                Invoke(new DrawFControlHandler(DrawFControl), new object[] { friend });
            }
            else
            {
                UIControls ui = new UIControls();
                Hashtable h = ui.GetFriendTypePanel(friend.ID, friend.FirstName + " " + friend.LastName, friend.Emial, ViaImage.ByteArrayToImage(friend.Image), imgListStatus.Images[2]);
                Panel p = (Panel)h["panel"];
                p.Click += FControlClick;
                Hashtable hRecent = ui.GetFriendTypePanel(friend.ID, friend.FirstName + " " + friend.LastName, friend.Emial, ViaImage.ByteArrayToImage(friend.Image), imgListStatus.Images[2]);
                Panel pRecent = (Panel)hRecent["panel"];
                pRecent.Click += FControlClick;
                lock (_floFriendListLock)
                {
                    floFriendList.Controls.Add(p);
                }
                lock (_contactListLock)
                {
                    _contactList[friend.ID].PnlContact = (Panel)h["panel"];
                    _contactList[friend.ID].OvlContactImage = (OvalShape)h["flistImg"];
                    _contactList[friend.ID].OvlContactStatus = (OvalShape)h["statusImg"];
                    Panel pnl = (Panel)h["notSeenCountPanel"];
                    pnl.Hide();
                    _contactList[friend.ID].PnlRecentContact = (Panel)hRecent["panel"];
                    _contactList[friend.ID].OvlRecentContact = (OvalShape)hRecent["statusImg"];
                    _contactList[friend.ID].PnlNotSeenCount = (Panel)hRecent["notSeenCountPanel"];
                    _contactList[friend.ID].LblNotSeenCount = (Label)hRecent["notSeenCountLable"];
                    _contactList[friend.ID].PnlNotSeenCount.Hide();
                }
            }
        }

        private void DrawMControl(ViaMessage msg, MessageArgs type, int msgId)
        {
            if (pnlMsg.InvokeRequired)
            {
                Invoke(new DrawMControlHandler(DrawMControl), new object[] { msg, type, msgId });
            }
            else
            {
                int id = type.Equals(MessageArgs.Received) ? msg.Sender : msg.Receiver;
                if (_contactList[id].PrevMsgType == type && _contactList[id].PrevMsgTime == Convert.ToInt64((msg.Date.ToString("yyyyMMddHHmm"))))
                {
                    _contactList[id].PrevMsg.Text += "\n" + msg.Message;
                }
                else
                {
                    UIControls ui = new UIControls();
                    Hashtable h = new Hashtable();
                    if (type.Equals(MessageArgs.Received))
                    {
                        h = ui.ReceiveMsgPanel(msgId, msg.Message, ViaImage.ByteArrayToImage(_contactList[msg.Sender].UserDetails.Image), msg.Date);
                        Panel pnlMessage = (Panel)h["MsgContainer"];
                        Label lblMessage = (Label)h["MsgContent"];
                        _contactList[msg.Sender].FlomsgContainer.Controls.Add(pnlMessage);
                        _contactList[msg.Sender].FlomsgContainer.ScrollControlIntoView(pnlMessage);
                        _contactList[msg.Sender].PrevMsg = lblMessage;
                        _contactList[msg.Sender].PrevMsgTime = Convert.ToInt64(msg.Date.ToString("yyyyMMddHHmm"));
                        _contactList[msg.Sender].PrevMsgType = MessageArgs.Received;
                    }
                    if (type.Equals(MessageArgs.Send))
                    {
                        h = ui.SendMsgPanel(msgId, msg.Message, msg.Date);
                        Panel pnlMessage = (Panel)h["MsgContainer"];
                        Label lblMessage = (Label)h["MsgContent"];
                        _contactList[msg.Receiver].FlomsgContainer.Controls.Add(pnlMessage);
                        _contactList[msg.Receiver].FlomsgContainer.ScrollControlIntoView(pnlMessage);
                        _contactList[msg.Receiver].FlomsgContainer.Controls.SetChildIndex(pnlMessage, msgId);
                        _contactList[msg.Receiver].PrevMsg = lblMessage;
                        _contactList[msg.Receiver].PrevMsgTime = Convert.ToInt64(msg.Date.ToString("yyyyMMddHHmm"));
                        _contactList[msg.Receiver].PrevMsgType = MessageArgs.Send;
                    }
                }
                _contactList[id].FlomsgContainer.VerticalScroll.Value = _contactList[id].FlomsgContainer.VerticalScroll.Maximum;
            }

        }

        //get friend's details when select friend from friend list
        private async void FControlClick(object sender, EventArgs e)
        {
            pnlType.Enabled = true;
            panel11.BackColor = Color.White;
            txtMsgContent.Enabled = true;
            Panel panel = (Panel)sender;
            int p = Convert.ToInt32(panel.Name);
            if (_selectedContactId != p)
            {
                if (_previousePanel != 0)
                {
                    _contactList[_previousePanel].PnlContact.BackColor = Color.White;
                    if (_contactList[_previousePanel].PnlRecentContact != null)
                    {
                        _contactList[_previousePanel].PnlRecentContact.BackColor = Color.DeepSkyBlue;
                    }
                }
                if (_contactList[p].PnlNotSeenCount.Visible == true)
                {
                    _contactList[p].PnlNotSeenCount.Hide();
                    _contactList[p].NotSeenMsgCount = 0;
                }
                if (_contactList[p].PnlRecentContact != null)
                {
                    _contactList[p].PnlRecentContact.BackColor = Color.DeepSkyBlue;
                }
                _contactList[p].PnlContact.BackColor = Color.DeepSkyBlue;
                this._previousePanel = p;
                _selectedContactId = _contactList[p].UserDetails.ID;
                SetSelectedContactDetails(ViaImage.ByteArrayToImage(_contactList[p].UserDetails.Image), _contactList[p].UserDetails.FirstName + " " + _contactList[p].UserDetails.LastName, _contactList[p].LoginStatus);

                pnlMsgWait.Show();
                await Task.Run(() => ShowMsgPanel()); // Relse main thred for 50 mili seconds
                if (_contactList[p].InitialMsg == false)
                {
                    Database db = new Database();
                    DataSet conversation = db.GetConversation(_fHandler.User.ID, _contactList[p].UserDetails.ID);
                    foreach (DataRow row in conversation.Tables[0].Rows)
                    {
                        ViaMessage msg = new ViaMessage();
                        msg.Sender = Convert.ToInt32(row["sender"]);
                        msg.Receiver = Convert.ToInt32(row["receiver"]);
                        msg.Date = Convert.ToDateTime(row["m_datetime"]);
                        msg.Message = row["m_content"].ToString();
                        if (msg.Sender == _fHandler.User.ID)
                        {
                            //MessageBox.Show("Load Send message from db - Receiver :" + msg.Receiver);
                            DrawMControl(msg, MessageArgs.Send, Convert.ToInt32(row["msg_id"]));
                        }
                        else
                        {
                            //MessageBox.Show("Load Received message from db - Sender :" + msg.Sender);
                            DrawMControl(msg, MessageArgs.Received, Convert.ToInt32(row["msg_id"]));
                        }
                    }
                    _contactList[p].InitialMsg = true;
                }
                pnlMsg.Controls.Clear();
                pnlMsg.Controls.Add(_contactList[p].FlomsgContainer);
                pnlMsgWait.Hide();
            }
            //txtRece.Text = _selectedContactId.ToString();
        }


        public void CheckClientStatus()
        {
            if (!timerCheckConnection.Enabled && UserLoginStatus == LoginStatus.Online)
            {
                timerCheckConnection.Start();
            }
            _connectionTimeOutCount = 0;
        }


        private void ChangeMode(LoginStatus status)
        {
            if (status.Equals(LoginStatus.Away))
            {
                Console.WriteLine("Mode change to away");
                picBoxUserStatus.BackgroundImage = imgListStatus.Images["away"];
                lblUserStatus.Text = "Away";
                foreach (int id in _contactList.Keys)
                {
                    SyncFriendsStatus(id, ViaService.LoginStatus.Invisible);
                }
            }
            if (status.Equals(LoginStatus.Online))
            {
                picBoxUserStatus.BackgroundImage = imgListStatus.Images["online"];
                lblUserStatus.Text = "Online";
                SyncMessage();
            }
            if (status.Equals(LoginStatus.Offline))
            {
                Console.WriteLine("Mode change to Offline");
                // this._ctsOnlineThread.Cancel();
                _fHandler.FormArgument = FormReturngArguments.SingOut;
                timerCheckConnection.Stop();
                this.Close();
            }
        }


        //receive friends login status
        //then it set to particular friend status control
        public void SyncFriendsStatus(int userId, ViaService.LoginStatus status)
        {
            lock (_contactListLock)
            {
                if (_contactList.ContainsKey(userId))
                {
                    _contactList[userId].LoginStatus = (LoginStatus)status;
                }
            }

        }

        public MessageArgs MessageReceived(ViaMessage msg)
        {
            Database db = new Database();
            int msgId = db.SaveMsg(msg.Sender, _fHandler.User.ID, msg.Date, msg.Message, (int)MessageArgs.Received);
            Task.Run(() => ReceiveMessageHandle(msg, msgId));
            return MessageArgs.Received;
        }

        private void ReceiveMessageHandle(ViaMessage msg, int msgId)
        {
            if (lblRecentCount.InvokeRequired | pnlRecentCount.InvokeRequired)
            {
                Invoke(new MessageReceiveHandler(ReceiveMessageHandle), new object[] { msg, msgId });
            }
            else
            {
                if (_recentList.Contains(msg.Sender))
                {
                    flopnlRecent.Controls.SetChildIndex(_contactList[msg.Sender].PnlRecentContact, 0);
                }
                if (!_recentList.Contains(msg.Sender))
                {
                    flopnlRecent.Controls.Add(_contactList[msg.Sender].PnlRecentContact);
                    _recentList.Add(msg.Sender);
                }
                if (_selectedContactId != msg.Sender)
                {
                    _contactList[msg.Sender].NotSeenMsgCount++;
                    _contactList[msg.Sender].LblNotSeenCount.Text = _contactList[msg.Sender].NotSeenMsgCount.ToString();
                    _contactList[msg.Sender].PnlNotSeenCount.Show();
                    if (_selectedPanel != PanelType.Recent)
                    {
                        if (!_notSeenMsgCount.Contains(msg.Sender))
                        {
                            _notSeenMsgCount.Add(msg.Sender);
                        }
                        lblRecentCount.Text = _notSeenMsgCount.Count.ToString();
                        if (pnlRecentCount.Visible == false)
                        {
                            pnlRecentCount.Show();
                        }
                    }
                }

                if (_contactList[msg.Sender].InitialMsg == true)
                {
                    DrawMControl(msg, MessageArgs.Received, msgId);
                }
            }
        }

        private void SignOut()
        {
            try
            {
                this._fHandler.Client.LogOut();
            }
            catch (Exception)
            {
                Console.Write("");
            }

            this._closingFlag = true;
            this.UserLoginStatus = LoginStatus.Offline;
        }


        public void SyncContacts(List<ViaFriend> friends)
        {
            Database db = new Database();

            foreach (ViaFriend friend in friends)
            {
                lock (_contactListLock)
                {
                    if (!_contactList.ContainsKey(friend.ID))
                    {
                        Contact contact = new Contact(friend);
                        //event that raise when contact status changed
                        contact.ContactStatusChange += contact_ContactStatusChange;
                        _contactList.Add(friend.ID, contact);
                        this.DrawFControl(friend);
                    }
                }
                ViaUser user = new ViaUser() { ID = friend.ID, FirstName = friend.FirstName, LastName = friend.LastName, Emial = friend.Emial, Image = friend.Image };

                if (!db.CheckUser(user.ID))
                {
                    db.SaveUser(user);
                }
                if (!db.CheckFriends(friend.Accepter, friend.Requester))
                {
                    db.SaveFriend(friend);
                }
            }
            Task t = Task.Run(() => FriendListRefreshRequest());
        }

        private void FriendListRefreshRequest()
        {
            _fHandler.Client.FriendListRefreshRequest();
        }

        //Start of Form Controls Decoration methods
        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing && this._closingFlag == false)
            {
                e.Cancel = true;
                this.WindowState = FormWindowState.Minimized;
            }
        }

        private async void pnlRecent_MouseClick(object sender, MouseEventArgs e)
        {
            if (_selectedPanel == PanelType.Search)
            {
                if (_selectedSearchContact != 0)
                {
                    SetSelectedContactDetails(ViaImage.ByteArrayToImage(_contactList[_selectedContactId].UserDetails.Image), _contactList[_selectedContactId].UserDetails.FirstName + " " + _contactList[_selectedContactId].UserDetails.LastName, _contactList[_selectedContactId].LoginStatus);
                    panel11.BackColor = Color.White;
                    pnlRecent.BackColor = Color.SkyBlue;
                    txtMsgContent.Enabled = true;
                }
                else
                {
                    SelectedContactPnelClear();
                }
            }
            if (_selectedPanel != PanelType.Recent)
            {
                pnlRecent.BackColor = Color.DeepSkyBlue;
                pnlSearch.BackColor = Color.DeepSkyBlue;
                pnlContact.BackColor = Color.DeepSkyBlue;
                flopnlRecent.Show();
                flopnlSearch.Hide();
                floFriendList.Hide();
                pnlSearchMessageContainer.Hide();
                _notSeenMsgCount.Clear();
                pnlRecentCount.Hide();
                if (_selectedPanel != PanelType.Contact)
                {
                    pnlMsgWait.Show();
                    await Task.Run(() => ShowMsgPanel());
                    pnlMsg.Show();
                    pnlMsgWait.Hide();
                }
                _selectedPanel = PanelType.Recent;
            }
        }

        private void SelectedContactPnelClear()
        {
            picSelectedUser.BackgroundImage = null;
            lblSelectUserName.Text = "";
            lblSelectUserStatus.Text = "";
            ovlSelectUserStatus.BackgroundImage = null;
        }

        //private void pnlRecent_MouseEnter(object sender, EventArgs e)
        //{
        //    if (_selectedPanel != PanelType.Recent)
        //    {
        //        pnlRecent.BackColor = Color.DeepSkyBlue;
        //    }
        //}

        private void pnlRecent_MouseLeave(object sender, EventArgs e)
        {
            if (_selectedPanel != PanelType.Recent)
            {
                pnlRecent.BackColor = Color.DeepSkyBlue;
            }
        }

        //private void pnlSearch_Enter(object sender, EventArgs e)
        //{
        //    pnlSearch.BackColor = Color.Red;
        //}

        //private void pnlSearch_Leave(object sender, EventArgs e)
        //{
        //    pnlSearch.BackColor = Color.White;
        //}


        //private void pnlContact_Leave(object sender, EventArgs e)
        //{
        //    pnlContact.BackColor = Color.DeepSkyBlue;
        //}

        private void pnlRecent_MouseEnter_1(object sender, EventArgs e)
        {
            if (_selectedPanel != PanelType.Recent)
            {
                pnlRecent.BackColor = Color.SkyBlue;
            }
        }

        private async void pnlContact_MouseClick(object sender, MouseEventArgs e)
        {
            if (_selectedPanel == PanelType.Search)
            {
                if (_selectedContactId != 0)
                {
                    SetSelectedContactDetails(ViaImage.ByteArrayToImage(_contactList[_selectedContactId].UserDetails.Image), _contactList[_selectedContactId].UserDetails.FirstName + " " + _contactList[_selectedContactId].UserDetails.LastName, _contactList[_selectedContactId].LoginStatus);
                    panel11.BackColor = Color.White;
                    txtMsgContent.Enabled = true;
                }
                else
                {
                    SelectedContactPnelClear();
                }
            }
            if (_selectedPanel != PanelType.Contact)
            {
                floFriendList.Show();
                pnlRecent.BackColor = Color.DeepSkyBlue;
                flopnlRecent.Hide();
                flopnlSearch.Hide();
                pnlSearchMessageContainer.Hide();
                pnlRecent.BackColor = Color.DeepSkyBlue;
                pnlSearch.BackColor = Color.DeepSkyBlue;
                if (_selectedPanel != PanelType.Recent)
                {
                    pnlMsgWait.Show();
                    await Task.Run(() => ShowMsgPanel());
                    pnlMsg.Show();
                    pnlMsgWait.Hide();
                }
                _selectedPanel = PanelType.Contact;
            }
        }

        private delegate Task<bool> ShowMsgPanelHandler();
        private Task<bool> ShowMsgPanel()
        {
            Thread.Sleep(50);
            return Task.FromResult<bool>(true);
        }

        private void pnlContact_MouseEnter(object sender, EventArgs e)
        {
            if (_selectedPanel != PanelType.Contact)
            {
                pnlContact.BackColor = Color.SkyBlue;
            }
        }

        private void pnlContact_MouseLeave(object sender, EventArgs e)
        {
            if (_selectedPanel != PanelType.Contact)
            {
                pnlContact.BackColor = Color.DeepSkyBlue;
            }
        }

        private void pnlSearch_MouseClick(object sender, MouseEventArgs e)
        {
            if (_selectedPanel != PanelType.Search)
            {
                if (_selectedSearchContact != 0)
                {
                    ViaFriend f = _searchContactList[_selectedSearchContact].UserDetails;
                    SetSelectedContactDetails(ViaImage.ByteArrayToImage(f.Image), f.FirstName + " " + f.LastName, LoginStatus.Invisible);
                }
                else if(_selectedSearchContact == 0)
                {
                    SelectedContactPnelClear();
                }
                else
                {
                    SelectedContactPnelClear();
                }
                panel11.BackColor = SystemColors.Control;
                txtMsgContent.Enabled = false;
                pnlSearch.BackColor = Color.SkyBlue;
                _selectedPanel = PanelType.Search;
                floFriendList.Hide();
                flopnlRecent.Hide();
                flopnlSearch.Show();
                pnlMsg.Hide();
                pnlSearchMessageContainer.Show();
                pnlRecent.BackColor = Color.DeepSkyBlue;
                pnlContact.BackColor = Color.DeepSkyBlue;
            }
        }

        private void pnlSearch_MouseEnter(object sender, EventArgs e)
        {
            if (_selectedPanel != PanelType.Search)
            {
                pnlSearch.BackColor = Color.SkyBlue;
            }
        }

        private void pnlSearch_MouseLeave(object sender, EventArgs e)
        {
            if (_selectedPanel != PanelType.Search)
            {
                pnlSearch.BackColor = Color.DeepSkyBlue;
            }
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            label1.BackColor = Color.DarkGreen;
            if (txtSearch.Text == "Search")
            {
                txtSearch.Text = "";
            }
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            label1.BackColor = Color.DarkSeaGreen;
            if (txtSearch.Text == "")
            {
                txtSearch.Text = "Search";
            }
        }
        private void panel12_MouseEnter(object sender, EventArgs e)
        {
            panel12.BorderStyle = BorderStyle.FixedSingle;
        }

        private void panel12_MouseLeave(object sender, EventArgs e)
        {
            panel12.BorderStyle = BorderStyle.None;
        }

        // End of Form Controls Decoration methods


        private void timerCheckConnection_Tick(object sender, EventArgs e)
        {
            _connectionTimeOutCount++;

            if (_connectionTimeOutCount > 100)
            {
                _connectionTimeOutCount = 0;
                this.TryingToConnect();
            }

        }

        private void SendMessageHandler(string msgContent)
        {
            Database db = new Database();
            ViaMessage msg = new ViaMessage();
            msg.Message = msgContent;
            msg.Sender = _fHandler.User.ID;
            msg.Receiver = _selectedContactId;
            msg.Date = DateTime.Now;
            int msgId = db.SaveMsg(msg.Sender, msg.Receiver, msg.Date, msg.Message, (int)MessageArgs.SendingFail);
            Task.Run(() => DrawMControl(msg, MessageArgs.Send, msgId));
            if (UserLoginStatus.Equals(LoginStatus.Online))
            {
                Task.Run(() => SendMessage(msg, msgId));
            }
        }

        //send sending faild message
        private void SyncMessage()
        {
            Database db = new Database();
            DataSet ds = db.GetMessagesByStatus((int)MessageArgs.SendingFail);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                ViaMessage msg = new ViaMessage();
                msg.Sender = Convert.ToInt32(row["sender"]);
                msg.Receiver = Convert.ToInt32(row["receiver"]);
                msg.Message = row["m_content"].ToString();
                msg.Date = Convert.ToDateTime(row["m_datetime"]);
                Task.Run(() => SendMessage(msg, Convert.ToInt32(row["msg_id"])));
            }

        }

        private async void SendMessage(ViaMessage msg, int msgId)
        {
            try
            {
                MessageArgs res = await _fHandler.Client.SendMsgAsync(msg);
                if (res.Equals(MessageArgs.Send))
                {
                    Database db = new Database();
                    db.UpdateMsgStatus((int)res, msgId);
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Sever connection is lost! Message will be send after reconnect to servie.");
            }
            
        }

        private void panel12_MouseClick(object sender, MouseEventArgs e)
        {
            if (txtMsgContent.Text.Trim() != "")
            {
                string msg = txtMsgContent.Text.Trim();
                txtMsgContent.Clear();
                Task.Run(() => SendMessageHandler(msg));
            }
        }

        private void txtMsgContent_KeyDown(object sender, KeyEventArgs e)
        {
            if (txtMsgContent.Text.Trim() != "" && e.KeyCode == Keys.Enter)
            {
                string msg = txtMsgContent.Text.Trim();
                txtMsgContent.Clear();
                Task.Run(() => SendMessageHandler(msg));
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {



            if (txtSearch.Text != "")
            {
                if (_searchTask != null && _searchTask.Status == TaskStatus.Running)
                {
                    _ctsSearch.Cancel();
                }
                string searchKey = txtSearch.Text;
                //_searchTask = Task.Run(() => Search(searchKey, _ctSearch), _ctSearch);
            }
            else
            {
                flopnlSearch.Controls.Clear();
                flopnlSearch.Controls.Add(pnlNoSearchResult);
            }


        }

        private void lblAddContactBtn_MouseEnter(object sender, EventArgs e)
        {
            ovlAddContactLeft.FillColor = Color.DarkCyan;
            ovlAddContactLeft.BorderColor = Color.DarkCyan;
            ovalAddContactRight.FillColor = Color.DarkCyan;
            ovalAddContactRight.BorderColor = Color.DarkCyan;
            lblAddContactBtn.BackColor = Color.DarkCyan;
        }

        private void lblAddContactBtn_MouseLeave(object sender, EventArgs e)
        {
            ovlAddContactLeft.FillColor = Color.LightSeaGreen;
            ovalAddContactRight.FillColor = Color.LightSeaGreen;
            ovalAddContactRight.FillColor = Color.LightSeaGreen;
            ovalAddContactRight.BorderColor = Color.LightSeaGreen;
            lblAddContactBtn.BackColor = Color.LightSeaGreen;
        }

        private void label3_MouseEnter(object sender, EventArgs e)
        {
            ovlAddContactMsgCancelBtnLeft.FillColor = Color.DarkCyan;
            ovlAddContactMsgCancelBtnRight.FillColor = Color.DarkCyan;
            ovlAddContactMsgCancelBtnRight.BorderColor = Color.DarkCyan;
            ovlAddContactMsgCancelBtnLeft.BorderColor = Color.DarkCyan;
            lblAddContactMsgCancelBtn.BackColor = Color.DarkCyan;
        }

        private void label3_MouseLeave(object sender, EventArgs e)
        {
            ovlAddContactMsgCancelBtnLeft.FillColor = Color.LightSeaGreen;
            ovlAddContactMsgCancelBtnRight.FillColor = Color.LightSeaGreen;
            ovlAddContactMsgCancelBtnRight.BorderColor = Color.LightSeaGreen;
            ovlAddContactMsgCancelBtnLeft.BorderColor = Color.LightSeaGreen;
            lblAddContactMsgCancelBtn.BackColor = Color.LightSeaGreen;
        }

        private void lblAddContactMsgSendButton_MouseEnter(object sender, EventArgs e)
        {
            lblAddContactMsgSendButton.BackColor = Color.DarkCyan;
            ovlAddContactMsgLeft.FillColor = Color.DarkCyan;
            ovlAddContactMsgRight.FillColor = Color.DarkCyan;
            ovlAddContactMsgRight.BorderColor = Color.DarkCyan;
            ovlAddContactMsgLeft.BorderColor = Color.DarkCyan;
        }

        private void lblAddContactMsgSendButton_MouseLeave(object sender, EventArgs e)
        {
            lblAddContactMsgSendButton.BackColor = Color.LightSeaGreen;
            ovlAddContactMsgLeft.FillColor = Color.LightSeaGreen;
            ovlAddContactMsgRight.FillColor = Color.LightSeaGreen;
            ovlAddContactMsgRight.BorderColor = Color.LightSeaGreen;
            ovlAddContactMsgLeft.BorderColor = Color.LightSeaGreen;
        }

        private void lblAddContactBtn_MouseClick(object sender, MouseEventArgs e)
        {
            if (pnlAddContactMsg.Visible == false)
            {
                pnlAddContactMsg.Show();
            }
        }

        private void lblAddContactMsgCancelBtn_MouseClick(object sender, MouseEventArgs e)
        {
            if (pnlAddContactMsg.Visible == true)
            {
                pnlAddContactMsg.Hide();
            }
        }

        private void lblAddContactMsgSendButton_Click(object sender, EventArgs e)
        {
            if (_userLoginStatus == LoginStatus.Online)
            {
                int receiveUserId = Convert.ToInt32(txtHideSearchContactId.Text);

                Contact nContact = _searchContactList[receiveUserId];
                string msg;
                if (txtAddContactMsg.Text.Trim() != "")
                {
                    msg = txtAddContactMsg.Text;
                }
                else
                {
                    msg = "Hi " + nContact.UserDetails.FirstName + " " + nContact.UserDetails.LastName + ", I'd like to add you as a contact.";
                }
                bool res = _fHandler.Client.AddContact(nContact.UserDetails.ID);
                if (res)
                {
                    Database db = new Database();
                    ViaFriend newFriend = new ViaFriend();
                    newFriend.ID = nContact.UserDetails.ID;
                    newFriend.FirstName = nContact.UserDetails.FirstName;
                    newFriend.LastName = nContact.UserDetails.LastName;
                    newFriend.Emial = nContact.UserDetails.Emial;
                    newFriend.Image = nContact.UserDetails.Image;
                    newFriend.Accepter = receiveUserId;
                    newFriend.Requester = _fHandler.User.ID;
                    newFriend.Date = DateTime.Now;
                    newFriend.Status = 1;
                    List<ViaFriend> fList = new List<ViaFriend>();
                    fList.Add(newFriend);
                    SyncContacts(fList);
                    Panel p = _contactList[receiveUserId].PnlContact;
                    _contactList[receiveUserId].InitialMsg = true;
                    FControlClick(p, new EventArgs());
                    Task.Run(() => SendMessageHandler(msg));
                    MessageBox.Show("Contact added to your contact list");
                    _fHandler.Client.FriendListRefreshRequest();
                    flopnlSearch.Controls.Remove(nContact.PnlContact);
                    pnlSearchMessageContainer.Controls.Clear();
                    _selectedSearchContact = 0;
                    SelectedContactPnelClear();

                }
            }
            else
            {
                MessageBox.Show("Can not add contact while not connect to Via Service");
            }

        }

        //drow search friend
        private void DrawSControl(ViaUser user)
        {
            if (flopnlSearch.InvokeRequired)
            {
                Invoke(new DrawSControlHandler(DrawSControl), new object[] { user });
            }
            else
            {
                UIControls ui = new UIControls();
                Hashtable h = ui.GetFriendTypePanel(user.ID, user.FirstName + " " + user.LastName, user.Emial, ViaImage.ByteArrayToImage(user.Image), imgListStatus.Images[(int)LoginStatus.Invisible]);
                Panel p = (Panel)h["panel"];
                Panel pnl = (Panel)h["notSeenCountPanel"];
                pnl.Hide();
                flopnlSearch.Controls.Add(p);
                if (_searchContactList.ContainsKey(user.ID))
                {
                    _searchContactList[user.ID].PnlContact = p;
                }
                p.Click += AddFriendPanel_Click;

            }
        }

        private void AddFriendPanel_Click(object sender, EventArgs e)
        {
            Panel p = (Panel)sender;
            if (_selectedSearchContact != 0)
            {
                _searchContactList[_selectedSearchContact].PnlContact.BackColor = Color.DeepSkyBlue;
            }
            p.BackColor = Color.DeepSkyBlue;
            _selectedSearchContact = Convert.ToInt32(p.Name);
            pnlAddContactMsg.Hide();
            ViaFriend newContact = _searchContactList[Convert.ToInt32(p.Name)].UserDetails;

            SetSelectedContactDetails(ViaImage.ByteArrayToImage(newContact.Image), newContact.FirstName + " " + newContact.LastName, LoginStatus.Invisible);
            txtHideSearchContactId.Text = newContact.ID.ToString();
            lblAddContact.Text = newContact.FirstName + " " + newContact.LastName + " not in your contacts";
            txtAddContactMsg.Text = "Hi " + newContact.FirstName + " " + newContact.LastName + ", I'd like to add you as a contact.";
            lblAddContactMsg.Text = "Send " + newContact.FirstName + " " + newContact.LastName + " a contact request";
            pnlSearchMessageContainer.Controls.Clear();
            pnlSearchMessageContainer.Controls.Add(pnlAddContactContainer);
        }

        private void SetSelectedContactDetails(Image image, string name, LoginStatus status)
        {
            picSelectedUser.BackgroundImage = image;
            lblSelectUserName.Text = name;
            ovlSelectUserStatus.BackgroundImage = imgListStatus.Images[(int)status];
            lblSelectUserStatus.Text = status.ToString();
            pnlVideoDial.Visible = true;

        }

        private async void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (txtSearch.Text.Trim() != "" && e.KeyCode == Keys.Enter)
            {
                if (_selectedPanel != PanelType.Search)
                {
                    panel11.BackColor = SystemColors.Control;
                    txtMsgContent.Enabled = false;
                    pnlRecent.BackColor = Color.DeepSkyBlue;
                    _selectedPanel = PanelType.Search;
                    floFriendList.Hide();
                    flopnlRecent.Hide();
                    flopnlSearch.Show();
                    pnlMsg.Hide();
                    pnlSearchMessageContainer.Show();
                    pnlContact.BackColor = Color.DeepSkyBlue;
                    pnlSearch.BackColor = Color.DeepSkyBlue;
                }
                SelectedContactPnelClear();
                _searchContactList.Clear();
                txtSearch.Enabled = false;
                flopnlSearch.Controls.Clear();
                flopnlSearch.Controls.Add(pnlSearching);
                pnlSearchMessageContainer.Controls.Clear();
                List<ViaUser> uList = await _fHandler.Client.SearchAsync(txtSearch.Text.Trim());
                flopnlSearch.Controls.Clear();
                if (uList.Count > 0)
                {
                    foreach (ViaUser user in uList)
                    {
                        ViaFriend friend = new ViaFriend();
                        friend.ID = user.ID;
                        friend.FirstName = user.FirstName;
                        friend.LastName = user.LastName;
                        friend.Emial = user.Emial;
                        friend.Image = user.Image;
                        Contact contact = new Contact(friend);
                        _searchContactList.Add(user.ID, contact);
                        DrawSControl(user);
                    }
                }
                else
                {
                    flopnlSearch.Controls.Add(pnlNoSearchResult);
                }
                txtSearch.Enabled = true;
                txtSearch.Focus();
            }
        }

//------------------------------------------------------------------------------//
//For Video Calling - Callback methods

       //WEBCAM----------------------------------------//

        private void ReleaseData()
        {
            if (_capture != null)
                _capture.Dispose();
        }

        private void ProcessFrame(object sender, EventArgs arg)
        {
            try
            {
                Framesno = _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_POS_FRAMES);
                frame = _capture.QueryFrame();

                if (frame != null)
                {
                    pictureBoxVideoIn.Image = frame.ToBitmap();
                    if (cam == 0)
                    {
                        double time_index = _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_POS_MSEC);

                        double framenumber = _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_POS_FRAMES);

                        Thread.Sleep((int)(1000.0 / FrameRate));
                    }

                    if (cam == 1)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        public void captureVideo()
        {
            #region cameracapture
            try
            {
                _capture = null;
                _capture = new Capture(0);
                _capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FPS, 30);
                _capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_HEIGHT, 240);
                _capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_WIDTH, 320);

                webcam_frm_cnt = 0;
                cam = 1;

                Application.Idle += ProcessFrame;
            }
            catch (NullReferenceException excpt)
            {
                MessageBox.Show(excpt.Message);
            }
            #endregion cameracapture
        }

        private void SendWebCamFrame()
        {
            MemoryStream ms = new MemoryStream();
            Image bmap;

            try
            {
                Thread videoThreadOne = new Thread(new ThreadStart(() => frame = _capture.QueryFrame()));
                videoThreadOne.Start();

                if (frame != null)
                {
                    bmap = ((Image)frame.ToBitmap());
                    bmap.Save(ms, ImageFormat.Jpeg);

                    byte[] buffer = ms.GetBuffer();
                    //video.PublishVideo(int.Parse(textBox2.Text), buffer);
                    try
                    {
                        Thread videoThreadTwo = new Thread(new ThreadStart(() => _fHandler.Client.PublishVideoAsync(_selectedContactId, buffer)));
                        videoThreadTwo.Start();
                    }
                    catch(Exception)
                    {
                        MessageBox.Show("Service Drop!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

       //---------------------------------------------//

        public void OnDial(int id, callType callType)
        {
            if (id == _fHandler.User.ID && callType == ViaService.callType.call)
            {
                //btnCutOut.Visible = false;
                picCutOut.Visible = false;
                lblCallStatus.Text = "Ringing...!";
                picStart.Visible = false;
                picAnswerIn.Visible = true;
                picCutIn.Visible = true;

                //Opening Window
                if (this.Width < 1000)
                {
                    this.Width = 1378;
                }

                //Incomming Call Sound
                //System.Media.SoundPlayer playerIncomeCall = new System.Media.SoundPlayer(@"C:\Users\ishar\OneDrive\Documents\VIa\ViaChatApplication\ChatApplication\Resources\ringSound.wav");
                //playerIncomeCall.Play();
            }
            else if (id == _fHandler.User.ID && callType == ViaService.callType.cut)
            {
                //btnCutOut.Visible = true;
                picCutOut.Visible = false;
                picAnswerIn.Visible = false;
                picCutIn.Visible = false;
                picStart.Visible = true;
                lblCallStatus.Text = "Call Ended....!";

                //Closing Window
                if(this.Width > 1000)
                {
                    this.Width = 865;
                }
            }
        }

        public void OnHold(int id, holdType holdType)
        {
            if(id ==_fHandler.User.ID && holdType == ViaService.holdType.unhold)
            {

              
            }
            else if(id == _fHandler.User.ID && holdType == ViaService.holdType.hold)
            {
                pictureBoxVideoOut.Image = Properties.Resources.video;
            }
        }

        public void OnVideo(int id, callType callType)
        {
            if (id == _fHandler.User.ID && callType == ViaService.callType.incomeAnswer)
            {
                //btnAnswerIn.Visible = true;
                //btnCutIn.Visible = true;
                pnlRing.Visible = false;
                lblCallStatus.Text = "";

                picAnswerIn.Visible = true;
                picCutIn.Visible = true;

                picManualStart.Visible = true;
                picStart.Visible = false;
                picStop.Visible = true;

                //Start Call From Answer End
                //iDevice = 0;
                //OpenPreviewWindow();
                timer1.Enabled = true;
                floFriendList.Enabled = false;
                panel3.Enabled = false;
            }
            else if (id == _fHandler.User.ID && callType == ViaService.callType.incomeCut)
            {
                //btnAnswerIn.Visible = true;
                //btnCutIn.Visible = true;
                lblCallStatus.Text = "Call Ended...!";

                ReleaseData();
                picCutOut.Visible = false;
                picStart.Visible = true;
            }
        }

        public void OnVideoSend(int id, byte[] data)
        {
            if (id == _fHandler.User.ID)
            {
                MemoryStream ms = new MemoryStream(data);
                pictureBoxVideoOut.Image = Image.FromStream(ms);
            }
        }

        public void OnCut(int id)
        {
            stopCapture();
            pictureBoxVideoOut.Image = Properties.Resources.video;
            pnlRing.Visible = true;
            defaultPanalSettings();
            lblCallStatus.Text = "Call Ended..!";
            picStart.Visible = true;
            floFriendList.Enabled = true;
            panel3.Enabled = true;
        }

        //Webcam API
        #region WebCam API
        //     const short WM_CAP = 1024;
        //     const int WM_CAP_DRIVER_CONNECT = WM_CAP + 10;
        //     const int WM_CAP_DRIVER_DISCONNECT = WM_CAP + 11;
        //     const int WM_CAP_EDIT_COPY = WM_CAP + 30;
        //     const int WM_CAP_SET_PREVIEW = WM_CAP + 50;
        //     const int WM_CAP_SET_PREVIEWRATE = WM_CAP + 52;
        //     const int WM_CAP_SET_SCALE = WM_CAP + 53;
        //     const int WS_CHILD = 1073741824;
        //     const int WS_VISIBLE = 268435456;
        //     const short SWP_NOMOVE = 2;
        //     const short SWP_NOSIZE = 1;
        //     const short SWP_NOZORDER = 4;
        //     const short HWND_BOTTOM = 1;
        //     int iDevice = 0;
        //     int hHwnd;
        //     [System.Runtime.InteropServices.DllImport("user32", EntryPoint = "SendMessageA")]
        //     static extern int SendMessage(int hwnd, int wMsg, int wParam, [MarshalAs(UnmanagedType.AsAny)] 
        //object lParam);
        //     [System.Runtime.InteropServices.DllImport("user32", EntryPoint = "SetWindowPos")]
        //     static extern int SetWindowPos(int hwnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);
        //     [System.Runtime.InteropServices.DllImport("user32")]
        //     static extern bool DestroyWindow(int hndw);
        //     [System.Runtime.InteropServices.DllImport("avicap32.dll")]
        //     static extern int capCreateCaptureWindowA(string lpszWindowName, int dwStyle, int x, int y, int nWidth, short nHeight, int hWndParent, int nID);
        //     [System.Runtime.InteropServices.DllImport("avicap32.dll")]
        //     static extern bool capGetDriverDescriptionA(short wDriver, string lpszName, int cbName, string lpszVer, int cbVer);

        //     private void OpenPreviewWindow()
        //     {
        //         int iHeight = pictureBoxVideoIn.Height;
        //         int iWidth = pictureBoxVideoIn.Width;
        //         // 
        //         //  Open Preview window in picturebox
        //         //  

        //         hHwnd = capCreateCaptureWindowA(iDevice.ToString(), (WS_VISIBLE | WS_CHILD), 0, 0, pictureBoxVideoIn.Image.Width, (short)pictureBoxVideoIn.Image.Height, pictureBoxVideoIn.Handle.ToInt32(), 0);
        //         // 
        //         //  Connect to device
        //         // 
        //         if (SendMessage(hHwnd, WM_CAP_DRIVER_CONNECT, iDevice, 0) == 1)
        //         {
        //             // 
        //             // Set the preview scale
        //             // 
        //             SendMessage(hHwnd, WM_CAP_SET_SCALE, 1, 0);
        //             // 
        //             // Set the preview rate in milliseconds
        //             // 
        //             SendMessage(hHwnd, WM_CAP_SET_PREVIEWRATE, 66, 0);
        //             // 
        //             // Start previewing the image from the camera
        //             // 
        //             SendMessage(hHwnd, WM_CAP_SET_PREVIEW, 1, 0);
        //             // 
        //             //  Resize window to fit in picturebox
        //             // 
        //             SetWindowPos(hHwnd, HWND_BOTTOM, 0, 0, iWidth, iHeight, (SWP_NOMOVE | SWP_NOZORDER));
        //         }
        //         else
        //         {
        //             // 
        //             //  Error connecting to device close window
        //             //  
        //             DestroyWindow(hHwnd);
        //         }

        //     }
        //private void ClosePreviewWindow()
        //{
        //    // 
        //    //  Disconnect from device
        //    // 
        //    SendMessage(hHwnd, WM_CAP_DRIVER_DISCONNECT, iDevice, 0);
        //    // 
        //    //  close window
        //    // 
        //    DestroyWindow(hHwnd);
        //    timer1.Dispose();
        //}
        #endregion

        private void timer1_Tick(object sender, EventArgs e)
        {
            //SendFrame();
            SendWebCamFrame();
        }

        //void SendFrame()
        //{
        //    MemoryStream ms = new MemoryStream();

        //    IDataObject data;
        //    Image bmap;

        //    //  Copy image to clipboard
        //    SendMessage(hHwnd, WM_CAP_EDIT_COPY, 0, 0);

        //    //  Get image from clipboard and convert it to a bitmap
        //    data = Clipboard.GetDataObject();

        //    if (data.GetDataPresent(typeof(System.Drawing.Bitmap)))
        //    {
        //        bmap = ((Image)(data.GetData(typeof(System.Drawing.Bitmap))));
        //        bmap.Save(ms, ImageFormat.Jpeg);
        //    }

        //    pictureBoxVideoIn.Image.Save(ms, ImageFormat.Jpeg);

        //    byte[] buffer = ms.GetBuffer();

        //    //video.PublishVideo(int.Parse(txtRece.Text), buffer);
        //    //video.PublishVideoAsync(int.Parse(txtRece.Text), buffer);
        //    _fHandler.Client.PublishVideoAsync(_selectedContactId, buffer);
        //}

        private void pnlVideoDial_Click(object sender, EventArgs e)
        {
            if (this.Width > 1000)
            {
                this.Width = 870;
            }
            else
            {
                this.Width = 1378;
            }
        }

        private void picAnswerIn_Click(object sender, EventArgs e)
        {
            captureVideo();
            pnlRing.Visible = false;

            picStop.Visible = true;
            //btnStart.Enabled = false;
            //btnStop.Enabled = false;

            //video.GetCall(int.Parse(txtRece.Text), callType.incomeAnswer);
            //video.GetCallAsync(int.Parse(txtRece.Text), callType.incomeAnswer);
            //_fHandler.Client.GetCallAsync(int.Parse(txtRece.Text), callType.incomeAnswer);
            _fHandler.Client.GetCallAsync(_selectedContactId, callType.incomeAnswer);

            picManualStart.Visible = true;

            //Start Video From Dial End
            //iDevice = 0;
            //OpenPreviewWindow();
            timer1.Enabled = true;
            floFriendList.Enabled = false;
            panel3.Enabled = false;
        }

        private void picCutIn_Click(object sender, EventArgs e)
        {
            lblCallStatus.Text = "Call Ended.....!";

            //video.GetCall(int.Parse(txtRece.Text), callType.incomeCut);
            //video.GetCallAsync(int.Parse(txtRece.Text), callType.incomeCut);
            //_fHandler.Client.GetCallAsync(int.Parse(txtRece.Text), callType.incomeCut);
            _fHandler.Client.GetCallAsync(_selectedContactId, callType.incomeCut);

            //btnCutOut.Visible = true;

            ReleaseData();
            picStart.Visible = true;
            picAnswerIn.Visible = false;
            picCutIn.Visible = false;

            if (this.Width > 1000)
            {
                this.Width = 865;
            }
            //btnStart.Enabled = true;
            //this.Close();
        }

        private void picCutOut_Click(object sender, EventArgs e)
        {
            lblCallStatus.Text = "Call Ended.....!";

            //video.GetCall(int.Parse(txtRece.Text), callType.cut);
            //video.GetCallAsync(int.Parse(txtRece.Text), callType.cut);
            //_fHandler.Client.GetCallAsync(int.Parse(txtRece.Text), callType.cut);
            _fHandler.Client.GetCallAsync(_selectedContactId, callType.cut);

            ReleaseData();
            picCutOut.Visible = false;
            picStart.Visible = true;

            if (this.Width > 1000)
            {
                this.Width = 865;
            }

            //btnCutIn.Visible = true;
            //btnAnswerIn.Visible = true;

            //btnStart.Enabled = true;
            //this.Close();
        }

        private void picManualStart_Click(object sender, EventArgs e)
        {
            //Start Video Call Manual
            if(timer1.Enabled == true)
            {
                timer1.Enabled = false;
                _fHandler.Client.HoldVideoAsync(_selectedContactId, holdType.hold);
            }
            else
            {
                timer1.Enabled = true;
                _fHandler.Client.HoldVideoAsync(_selectedContactId, holdType.unhold);
            }
        }

        private void picStop_Click(object sender, EventArgs e)
        {
            //ClosePreviewWindow();
            stopCapture();
            timer1.Enabled = false;
            Task CallCutTask = Task.Run(() => _fHandler.Client.CutCall(_selectedContactId));
            pnlRing.Visible = true;
            defaultPanalSettings();
            picStart.Visible = true;
            this.Width = 870;
            floFriendList.Enabled = true;
            panel3.Enabled = true;
            //this.Close();
        }

        public void stopCapture()
        {
            _capture.Stop();
            Application.Idle -= ProcessFrame;
            ReleaseData();
            if (cam == 1)
            {
                _capture.Dispose();
                cam = 0;
            }
        }

        private void picStart_Click(object sender, EventArgs e)
        {
            captureVideo();
            //video.GetCall(int.Parse(txtRece.Text), callType.call);
            //video.GetCallAsync(int.Parse(txtRece.Text), callType.call);
            //_fHandler.Client.GetCallAsync(int.Parse(txtRece.Text), callType.call);
            _fHandler.Client.GetCallAsync(_selectedContactId, callType.call);

            //btnAnswerIn.Visible = false;
            //btnCutIn.Visible = false;
            //btnStart.Enabled = false;
            //btnStop.Enabled = false;
            picAnswerIn.Visible = false;
            picCutIn.Visible = false;

            picCutOut.Visible = true;

            lblCallStatus.Text = "Calling...!";
            picStart.Visible = false;

            //Ringing sounds
            //getCallSound();
        }

        public void getCallSound()
        {
            System.Media.SoundPlayer playerGetCall = new System.Media.SoundPlayer(@"C:\Users\ishar\OneDrive\Documents\VIa\ViaChatApplication\ChatApplication\Resources\CallSound.wav");
            playerGetCall.Play();
        }

        private void panel3_Click(object sender, EventArgs e)
        {
            //Stop video call if still running. 
            if (timer1.Enabled == true)
            {
                timer1.Enabled = false;
            }
            SignOut();
        }
    }
}
