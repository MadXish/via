﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic.PowerPacks;
using System.Drawing;
using System.Collections;

namespace ChatApplication
{
    public class UIControls
    {

        public Hashtable GetFriendTypePanel(int id, string name,string email, Image image, Image statusImg)
        {
            Panel panel3 = new Panel();
            Label lblFriendListName = new Label();
            ShapeContainer shapeContainer2 =  new ShapeContainer();
            OvalShape imgFriendListStatus = new OvalShape();
            OvalShape imgFriendListImage = new OvalShape();
            Label lblContactEmail = new Label();
            OvalShape ovalpnlContactNotSeenMsgCount = new OvalShape();
            Panel pnlContactNotSeenMsgCount = new Panel();
            Label lblpnlContactNotSeenMsgCount = new Label();
            ShapeContainer shapeContainer6 = new ShapeContainer();

            // 
            // lblContactEmail
            // 
            lblContactEmail.AutoSize = true;
            lblContactEmail.Location = new System.Drawing.Point(69, 35);
            lblContactEmail.Name = "lblContactEmail" + id;
            lblContactEmail.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblContactEmail.ForeColor = Color.DarkCyan;
            lblContactEmail.Size = new System.Drawing.Size(140, 13);
            lblContactEmail.TabIndex = 1;
            lblContactEmail.Text = email;
            // 
            // panel3
            // 
            panel3.Controls.Add(lblFriendListName);
            panel3.Controls.Add(shapeContainer2);
            panel3.Controls.Add(lblContactEmail);
            panel3.Controls.Add(pnlContactNotSeenMsgCount);
            panel3.Location = new System.Drawing.Point(3, 3);
            panel3.Name = id.ToString();
            panel3.Size = new System.Drawing.Size(246, 66);
            panel3.TabIndex = 0;
            // 
            // imgFriendListImage
            // 
            imgFriendListImage.BackgroundImage = image;
            imgFriendListImage.Enabled = false;
            imgFriendListImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            imgFriendListImage.BorderColor = System.Drawing.Color.Transparent;
            imgFriendListImage.FillColor = System.Drawing.Color.DeepSkyBlue;
            imgFriendListImage.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            imgFriendListImage.BorderColor = Color.MintCream;
            imgFriendListImage.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            imgFriendListImage.Location = new System.Drawing.Point(5, 7);
            imgFriendListImage.Name = "imgFriendListImage" + id;
            imgFriendListImage.SelectionColor = System.Drawing.Color.Transparent;
            imgFriendListImage.Size = new System.Drawing.Size(50, 50);
            // 
            // shapeContainer2
            // 
            shapeContainer2.Location = new System.Drawing.Point(0, 0);
            shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            shapeContainer2.Name = "shapeContainer2" + id;
            shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            imgFriendListStatus,
            imgFriendListImage});
            shapeContainer2.Size = new System.Drawing.Size(246, 66);
            shapeContainer2.TabIndex = 0;
            shapeContainer2.TabStop = false;
            // 
            // imgFriendListStatus
            // 
            imgFriendListStatus.BackgroundImage = statusImg;
            imgFriendListStatus.Enabled = false;
            imgFriendListStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            imgFriendListStatus.BorderColor = System.Drawing.Color.MintCream;
            imgFriendListStatus.BorderWidth = 2;
            imgFriendListStatus.Location = new System.Drawing.Point(39, 42);
            imgFriendListStatus.Name = "imgFriendListStatus" + id;
            imgFriendListStatus.SelectionColor = System.Drawing.Color.Transparent;
            imgFriendListStatus.Size = new System.Drawing.Size(16, 16);
            // 
            // lblFriendListName
            // 
            lblFriendListName.AutoSize = true;
            lblFriendListName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblFriendListName.Location = new System.Drawing.Point(69, 18);
            lblFriendListName.Name = "lblFriendListName" + id;
            lblFriendListName.Size = new System.Drawing.Size(113, 16);
            lblFriendListName.TabIndex = 1;
            lblFriendListName.Text = name;
                        // 
                        // pnlContactNotSeenMsgCount
                        // 
            pnlContactNotSeenMsgCount.BackColor = System.Drawing.Color.Transparent;
            pnlContactNotSeenMsgCount.Controls.Add(lblpnlContactNotSeenMsgCount);
            pnlContactNotSeenMsgCount.Controls.Add(shapeContainer6);
            pnlContactNotSeenMsgCount.Location = new System.Drawing.Point(212, 3);
            pnlContactNotSeenMsgCount.Name = "pnlContactNotSeenMsgCount";
            pnlContactNotSeenMsgCount.Size = new System.Drawing.Size(27, 27);
            pnlContactNotSeenMsgCount.TabIndex = 3;
                   // 
                   // lblpnlContactNotSeenMsgCount
                   // 
            lblpnlContactNotSeenMsgCount.AutoSize = true;
            lblpnlContactNotSeenMsgCount.BackColor = System.Drawing.Color.Orange;
            lblpnlContactNotSeenMsgCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblpnlContactNotSeenMsgCount.Location = new System.Drawing.Point(7, 6);
            lblpnlContactNotSeenMsgCount.Name = "lblpnlContactNotSeenMsgCount";
            lblpnlContactNotSeenMsgCount.Size = new System.Drawing.Size(14, 13);
            lblpnlContactNotSeenMsgCount.TabIndex = 0;
            lblpnlContactNotSeenMsgCount.Text = "5";
                   // 
                   // shapeContainer6
                   // 
            shapeContainer6.Location = new System.Drawing.Point(0, 0);
            shapeContainer6.Margin = new System.Windows.Forms.Padding(0);
            shapeContainer6.Name = "shapeContainer6";
            shapeContainer6.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            ovalpnlContactNotSeenMsgCount});
            shapeContainer6.Size = new System.Drawing.Size(27, 27);
            shapeContainer6.TabIndex = 0;
            shapeContainer6.TabStop = false;
                   // 
                   // ovalpnlContactNotSeenMsgCount
                   // 
            ovalpnlContactNotSeenMsgCount.BackColor = System.Drawing.Color.DeepSkyBlue;
            ovalpnlContactNotSeenMsgCount.BorderColor = System.Drawing.Color.DarkOrange;
            ovalpnlContactNotSeenMsgCount.BorderWidth = 2;
            ovalpnlContactNotSeenMsgCount.FillColor = System.Drawing.Color.Orange;
            ovalpnlContactNotSeenMsgCount.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            ovalpnlContactNotSeenMsgCount.Location = new System.Drawing.Point(4, 3);
            ovalpnlContactNotSeenMsgCount.Name = "ovalpnlContactNotSeenMsgCount";
            ovalpnlContactNotSeenMsgCount.Size = new System.Drawing.Size(18, 18);



            Hashtable h = new Hashtable();
            h["panel"] = panel3;
            h["statusImg"] = imgFriendListStatus;
            h["flistImg"] = imgFriendListImage;
            h["flistlblName"] = lblFriendListName;
            h["notSeenCountLable"] = lblpnlContactNotSeenMsgCount;
            h["notSeenCountPanel"] = pnlContactNotSeenMsgCount;
            return h;
        }

        public Hashtable SendMsgPanel(int id,string msg,DateTime date)
        {
            Panel pnlSendMsgContainer = new Panel();
            Label lblSendMsgTime = new Label();
            Panel pnlSendMsgContent = new Panel();
            Label lblSendMsgDate = new Label();
            Label lblSendMsgContent = new Label();
            ShapeContainer shapeContainer2 = new ShapeContainer();
            OvalShape ovalShape4 = new OvalShape();


            // 
            // pnlSendMsgContainer
            // 
            pnlSendMsgContainer.AutoSize = true;
            pnlSendMsgContainer.BackColor = System.Drawing.Color.White;
            pnlSendMsgContainer.Controls.Add(lblSendMsgTime);
            pnlSendMsgContainer.Controls.Add(pnlSendMsgContent);
            pnlSendMsgContainer.Controls.Add(shapeContainer2);
            pnlSendMsgContainer.Location = new System.Drawing.Point(3, 62);
            pnlSendMsgContainer.Name = "pnlSendMsgContainer" + id;
            pnlSendMsgContainer.Size = new System.Drawing.Size(557, 53);
            pnlSendMsgContainer.TabIndex = 1;
            // 
            // lblSendMsgTime
            // 
            lblSendMsgTime.AutoSize = true;
            lblSendMsgTime.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblSendMsgTime.ForeColor = System.Drawing.Color.DarkSlateGray;
            lblSendMsgTime.Location = new System.Drawing.Point(513, 22);
            lblSendMsgTime.Name = "lblSendMsgTime" + id;
            lblSendMsgTime.Size = new System.Drawing.Size(41, 12);
            lblSendMsgTime.TabIndex = 3;
            lblSendMsgTime.Text = date.ToString("hh:mm tt");
            // 
            // pnlSendMsgContent
            // 
            pnlSendMsgContent.AutoSize = true;
            pnlSendMsgContent.BackColor = System.Drawing.Color.DeepSkyBlue;
            pnlSendMsgContent.Controls.Add(lblSendMsgDate);
            pnlSendMsgContent.Controls.Add(lblSendMsgContent);
            pnlSendMsgContent.Location = new System.Drawing.Point(156, 9);
            pnlSendMsgContent.Name = "pnlSendMsgContent" + id;
            pnlSendMsgContent.Size = new System.Drawing.Size(341, 41);
            pnlSendMsgContent.TabIndex = 1;
            // 
            // lblSendMsgDate
            // 
            lblSendMsgDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            lblSendMsgDate.AutoSize = true;
            lblSendMsgDate.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblSendMsgDate.ForeColor = System.Drawing.Color.SaddleBrown;
            lblSendMsgDate.Location = new System.Drawing.Point(255, 26);
            lblSendMsgDate.Name = "lblSendMsgDate" + id;
            lblSendMsgDate.Size = new System.Drawing.Size(69, 14);
            lblSendMsgDate.TabIndex = 2;
            lblSendMsgDate.Text = date.ToString("MMMM dd, yyyy");
            // 
            // lblSendMsgContent
            // 
            lblSendMsgContent.AutoSize = true;
            lblSendMsgContent.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblSendMsgContent.Location = new System.Drawing.Point(8, 10);
            lblSendMsgContent.MaximumSize = new System.Drawing.Size(333, 0);
            lblSendMsgContent.Name = "lblSendMsgContent" + id;
            lblSendMsgContent.Padding = new System.Windows.Forms.Padding(0, 0, 0, 13);
            lblSendMsgContent.Size = new System.Drawing.Size(130, 30);
            lblSendMsgContent.TabIndex = 1;
            lblSendMsgContent.Text = msg;
            // 
            // shapeContainer2
            // 
            shapeContainer2.Location = new System.Drawing.Point(0, 0);
            shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            shapeContainer2.Name = "shapeContainer2" + id;
            shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            ovalShape4});
            shapeContainer2.Size = new System.Drawing.Size(557, 53);
            shapeContainer2.TabIndex = 0;
            shapeContainer2.TabStop = false;
            // 
            // ovalShape4
            // 
            ovalShape4.BackColor = System.Drawing.Color.DeepSkyBlue;
            ovalShape4.BorderColor = System.Drawing.Color.DeepSkyBlue;
            ovalShape4.FillColor = System.Drawing.Color.DeepSkyBlue;
            ovalShape4.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            ovalShape4.Location = new System.Drawing.Point(492, 22);
            ovalShape4.Name = "ovalShape3" + id;
            ovalShape4.Size = new System.Drawing.Size(10, 10);
            Hashtable h = new Hashtable();
            h.Add("MsgContainer", pnlSendMsgContainer);
            h.Add("MsgContent", lblSendMsgContent);
            return h;
        }

        public Hashtable ReceiveMsgPanel(int id,string msg, Image image,DateTime date)
        {
            Panel pnlReceiveMsgContainer = new Panel();
            Label lblReceiveMsgTime = new Label();
            Label lblReceiveMsgDate = new Label();
            Label lblReceiveMsg = new Label();
            Panel pnlReceiveMsgContent = new Panel();
            ShapeContainer shapeContainer1 = new ShapeContainer();
            OvalShape ovalShape3 = new OvalShape();
            OvalShape receiveMsgImage = new OvalShape();


            // 
            // pnlReceiveMsgContainer
            // 
            pnlReceiveMsgContainer.AutoSize = true;
            pnlReceiveMsgContainer.BackColor = System.Drawing.Color.White;
            pnlReceiveMsgContainer.Controls.Add(lblReceiveMsgTime);
            pnlReceiveMsgContainer.Controls.Add(pnlReceiveMsgContent);
            pnlReceiveMsgContainer.Controls.Add(shapeContainer1);
            pnlReceiveMsgContainer.Location = new System.Drawing.Point(3, 3);
            pnlReceiveMsgContainer.Name = "pnlReceiveMsgContainer"+id;
            pnlReceiveMsgContainer.Size = new System.Drawing.Size(557, 53);
            pnlReceiveMsgContainer.TabIndex = 0;
            // 
            // lblReceiveMsgTime
            // 
            lblReceiveMsgTime.AutoSize = true;
            lblReceiveMsgTime.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblReceiveMsgTime.ForeColor = System.Drawing.Color.DarkSlateGray;
            lblReceiveMsgTime.Location = new System.Drawing.Point(513, 23);
            lblReceiveMsgTime.Name = "lblReceiveMsgTime" + id;
            lblReceiveMsgTime.Size = new System.Drawing.Size(41, 12);
            lblReceiveMsgTime.TabIndex = 2;
            lblReceiveMsgTime.Text = date.ToString("hh:mm tt"); 
            // 
            // pnlReceiveMsgContent
            // 
            pnlReceiveMsgContent.AutoSize = true;
            pnlReceiveMsgContent.BackColor = System.Drawing.Color.Silver;
            pnlReceiveMsgContent.Controls.Add(lblReceiveMsgDate);
            pnlReceiveMsgContent.Controls.Add(lblReceiveMsg);
            pnlReceiveMsgContent.Location = new System.Drawing.Point(98, 9);
            pnlReceiveMsgContent.Name = "pnlReceiveMsgContent" + id;
            pnlReceiveMsgContent.Size = new System.Drawing.Size(341, 41);
            pnlReceiveMsgContent.TabIndex = 1;
            // 
            // lblReceiveMsgDate
            // 
            lblReceiveMsgDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            lblReceiveMsgDate.AutoSize = true;
            lblReceiveMsgDate.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblReceiveMsgDate.ForeColor = System.Drawing.Color.SaddleBrown;
            lblReceiveMsgDate.Location = new System.Drawing.Point(255, 26);
            lblReceiveMsgDate.Name = "lblReceiveMsgDate" + id;
            lblReceiveMsgDate.Size = new System.Drawing.Size(69, 14);
            lblReceiveMsgDate.TabIndex = 1;
            lblReceiveMsgDate.Text = date.ToString("MMMM dd, yyyy");
            // 
            // lblReceiveMsg
            // 
            lblReceiveMsg.AutoSize = true;
            lblReceiveMsg.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblReceiveMsg.Location = new System.Drawing.Point(8, 11);
            lblReceiveMsg.MaximumSize = new System.Drawing.Size(333, 0);
            lblReceiveMsg.Name = "lblReceiveMsg" + id;
            lblReceiveMsg.Padding = new System.Windows.Forms.Padding(0, 0, 0, 13);
            lblReceiveMsg.Size = new System.Drawing.Size(130, 30);
            lblReceiveMsg.TabIndex = 0;
            lblReceiveMsg.Text = msg;
            // 
            // shapeContainer1
            // 
            shapeContainer1.Location = new System.Drawing.Point(0, 0);
            shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            shapeContainer1.Name = "shapeContainer1" + id;
            shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            ovalShape3,
            receiveMsgImage});
            shapeContainer1.Size = new System.Drawing.Size(557, 53);
            shapeContainer1.TabIndex = 0;
            shapeContainer1.TabStop = false;
            // 
            // ovalShape3
            // 
            ovalShape3.BackColor = System.Drawing.Color.Silver;
            ovalShape3.BorderColor = System.Drawing.Color.Silver;
            ovalShape3.FillColor = System.Drawing.Color.Silver;
            ovalShape3.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            ovalShape3.Location = new System.Drawing.Point(92, 21);
            ovalShape3.Name = "ovalShape3" + id;
            ovalShape3.Size = new System.Drawing.Size(10, 10);
            // 
            // receiveMsgImage
            // 
            receiveMsgImage.BackgroundImage = image;
            receiveMsgImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            receiveMsgImage.BorderColor = System.Drawing.Color.White;
            receiveMsgImage.Location = new System.Drawing.Point(39, 9);
            receiveMsgImage.Name = "receiveMsgImage" + id;
            receiveMsgImage.SelectionColor = System.Drawing.Color.Transparent;
            receiveMsgImage.Size = new System.Drawing.Size(40, 40);
            Hashtable h = new Hashtable();
            h.Add("MsgContainer", pnlReceiveMsgContainer);
            h.Add("MsgContent", lblReceiveMsg);
            return h;
        }

        public FlowLayoutPanel GetMsgContainer(int id)
        {
            FlowLayoutPanel floMsgContainer = new FlowLayoutPanel();
            // 
            // floMsgContainer
            // 
            floMsgContainer.AutoScroll = true;
            floMsgContainer.BackColor = System.Drawing.Color.White;
            floMsgContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            floMsgContainer.Location = new System.Drawing.Point(0, 0);
            floMsgContainer.Name = "floMsgContainer"+ id;
            floMsgContainer.Size = new System.Drawing.Size(598, 385);
            return floMsgContainer;
        }
    }
}
