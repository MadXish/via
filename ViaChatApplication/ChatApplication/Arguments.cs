﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApplication
{
    public enum FormReturngArguments
    {
        Close = 1,
        Error = 0,
        SingOut = 2,
        Login = 3
    }
    public enum LoginStatus
    {
        Online = 0,
        Away = 1,
        Invisible = 2,
        Offline = 3
    }

    public enum PanelType
    {
        Recent,
        Contact,
        Search
    }
}
