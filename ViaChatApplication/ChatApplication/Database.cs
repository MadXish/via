﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApplication
{
    public class Database
    {
        private string cs = "Data Source=ISHARA-PC\\SQLEXPRESS;Initial Catalog=TexyClientDB;Integrated Security=True";
        public Database() 
        {
        }
        public Database(string conString)
        {
            cs = conString;
        }

        public DataSet GetUserDetails(string email, string pass)
        {
            SqlDataAdapter adt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spGetUser", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramEmail = new SqlParameter("@email", email);
            cmd.Parameters.Add(pramEmail);
            SqlParameter pramPass = new SqlParameter("@pass", pass);
            cmd.Parameters.Add(pramPass);
            cn.Open();
            adt.SelectCommand = cmd;
            adt.Fill(ds);
            cn.Close();
            return ds;
        }

        public DataSet GetFriendList(int userId)
        {
            SqlDataAdapter adt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spGetFriends", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramId = new SqlParameter("@id", userId);
            cmd.Parameters.Add(pramId);
            cn.Open();
            adt.SelectCommand = cmd;
            adt.Fill(ds);
            cn.Close();
            return ds;
        }

        public bool CheckUser(int id)
        {
            SqlDataAdapter adt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spUserCheck", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramId = new SqlParameter("@id", id);
            cmd.Parameters.Add(pramId);
            cn.Open();
            adt.SelectCommand = cmd;
            adt.Fill(ds);
            cn.Close();
            if (Convert.ToInt32(ds.Tables[0].Rows[0]["countt"]) <= 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveUser(ViaService.ViaUser user)
        {
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spSaveUser", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramId = new SqlParameter("@id", user.ID);
            cmd.Parameters.Add(pramId);
            SqlParameter pramFName = new SqlParameter("@f_name", user.FirstName);
            cmd.Parameters.Add(pramFName);
            SqlParameter pramLName = new SqlParameter("@l_name", user.LastName);
            cmd.Parameters.Add(pramLName);
            SqlParameter pramEmail = new SqlParameter("@email", user.Emial);
            cmd.Parameters.Add(pramEmail);
            SqlParameter pramImage = new SqlParameter("@image", user.Image);
            cmd.Parameters.Add(pramImage);
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public bool CheckFriends(int accepterId,int requesterId)
        {
            SqlDataAdapter adt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spCheckFriend", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramAccepterId = new SqlParameter("@accepterId", accepterId);
            cmd.Parameters.Add(pramAccepterId);
            SqlParameter pramRequesterId = new SqlParameter("@requesterId", requesterId);
            cmd.Parameters.Add(pramRequesterId);
            cn.Open();
            adt.SelectCommand = cmd;
            adt.Fill(ds);
            cn.Close();
            if (Convert.ToInt32(ds.Tables[0].Rows[0]["countt"]) <= 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveFriend(ViaService.ViaFriend friend)
        {
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spSaveFriend", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramRequesterId = new SqlParameter("@requester", friend.Requester);
            cmd.Parameters.Add(pramRequesterId);
            SqlParameter pramAccepterId = new SqlParameter("@accepter",friend.Accepter);
            cmd.Parameters.Add(pramAccepterId);
            SqlParameter pramDate = new SqlParameter("@request_date", friend.Date);
            cmd.Parameters.Add(pramDate);
            SqlParameter pramStatus = new SqlParameter("@f_status", friend.Status);
            cmd.Parameters.Add(pramStatus);
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public int SaveMsg(int sender, int receiver, DateTime date, string msg, int status)
        {
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spSaveMsg", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramSender = new SqlParameter("@sender",sender);
            cmd.Parameters.Add(pramSender);
            SqlParameter pramReceiver = new SqlParameter("@receiver", receiver);
            cmd.Parameters.Add(pramReceiver);
            SqlParameter pramDate = new SqlParameter("@mDate", date);
            cmd.Parameters.Add(pramDate);
            SqlParameter pramContent = new SqlParameter("@mContent", msg);
            cmd.Parameters.Add(pramContent);
            SqlParameter pramStatus = new SqlParameter("@mStatus", status);
            cmd.Parameters.Add(pramStatus);
            cn.Open();
            int id = (int)cmd.ExecuteScalar();
            cn.Close();
            return id;
        }

        public void UpdateMsgStatus(int status, int msgId)
        {
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spChangeMsgStatus", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramStatus = new SqlParameter("@status", status);
            cmd.Parameters.Add(pramStatus);
            SqlParameter pramMsgId = new SqlParameter("@msgId", msgId);
            cmd.Parameters.Add(pramMsgId);
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public DataSet GetConversation(int sender, int receiver)
        {
            SqlDataAdapter adt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spGetConversation", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramSender = new SqlParameter("@sender", sender);
            cmd.Parameters.Add(pramSender);
            SqlParameter pramReceiver = new SqlParameter("@receiver", receiver);
            cmd.Parameters.Add(pramReceiver);
            cn.Open();
            adt.SelectCommand = cmd;
            adt.Fill(ds);
            cn.Close();
            return ds;
        }

        public DataSet GetMessagesByStatus(int status)
        {
            SqlDataAdapter adt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spGetMessages", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramSender = new SqlParameter("@mStatus",status);
            cmd.Parameters.Add(pramSender);
            cn.Open();
            adt.SelectCommand = cmd;
            adt.Fill(ds);
            cn.Close();
            return ds;
        }
    }
}
