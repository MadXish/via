﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ServiceModel;
using VideoClient.ViaService;
using System.Runtime.InteropServices;
using System.IO;
using System.Drawing.Imaging;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Threading;
using System.Timers;

namespace VideoClient
{
    [CallbackBehavior(UseSynchronizationContext = false)]
    public partial class Form1 : Form, IVideoClassLibraryCallback
    {
        ViaService.IVideoClassLibrary video;
        public Form1()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
            video = new ViaService.VideoClassLibraryClient(new InstanceContext(this), "NetTcpBinding_IVideoClassLibrary");
            lblNo.Text = video.Subscribe().ToString();

            pnlRing.Enabled = false;
            lblCallStatus.Text = "";
        }

        //Webcam API
        #region WebCam API
        const short WM_CAP = 1024;
        const int WM_CAP_DRIVER_CONNECT = WM_CAP + 10;
        const int WM_CAP_DRIVER_DISCONNECT = WM_CAP + 11;
        const int WM_CAP_EDIT_COPY = WM_CAP + 30;
        const int WM_CAP_SET_PREVIEW = WM_CAP + 50;
        const int WM_CAP_SET_PREVIEWRATE = WM_CAP + 52;
        const int WM_CAP_SET_SCALE = WM_CAP + 53;
        const int WS_CHILD = 1073741824;
        const int WS_VISIBLE = 268435456;
        const short SWP_NOMOVE = 2;
        const short SWP_NOSIZE = 1;
        const short SWP_NOZORDER = 4;
        const short HWND_BOTTOM = 1;
        int iDevice = 0;
        int hHwnd;
        [System.Runtime.InteropServices.DllImport("user32", EntryPoint = "SendMessageA")]
        static extern int SendMessage(int hwnd, int wMsg, int wParam, [MarshalAs(UnmanagedType.AsAny)] 
			object lParam);
        [System.Runtime.InteropServices.DllImport("user32", EntryPoint = "SetWindowPos")]
        static extern int SetWindowPos(int hwnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);
        [System.Runtime.InteropServices.DllImport("user32")]
        static extern bool DestroyWindow(int hndw);
        [System.Runtime.InteropServices.DllImport("avicap32.dll")]
        static extern int capCreateCaptureWindowA(string lpszWindowName, int dwStyle, int x, int y, int nWidth, short nHeight, int hWndParent, int nID);
        [System.Runtime.InteropServices.DllImport("avicap32.dll")]
        static extern bool capGetDriverDescriptionA(short wDriver, string lpszName, int cbName, string lpszVer, int cbVer);

        private void OpenPreviewWindow()
        {
            Console.WriteLine("OpenPreviewWindow get call - " + lblNo.Text);
            int iHeight = pictureBox1.Height;
            int iWidth = pictureBox1.Width;
            // 
            //  Open Preview window in picturebox
            //  

            hHwnd = capCreateCaptureWindowA(iDevice.ToString(), (WS_VISIBLE | WS_CHILD), 0, 0, pictureBox1.Image.Width, (short)pictureBox1.Image.Height, pictureBox1.Handle.ToInt32(), 0);
            // 
            //  Connect to device
            // 
            if (SendMessage(hHwnd, WM_CAP_DRIVER_CONNECT, iDevice, 0) == 1)
            {
                // 
                // Set the preview scale
                // 
                SendMessage(hHwnd, WM_CAP_SET_SCALE, 1, 0);
                // 
                // Set the preview rate in milliseconds
                // 
                SendMessage(hHwnd, WM_CAP_SET_PREVIEWRATE, 66, 0);
                // 
                // Start previewing the image from the camera
                // 
                SendMessage(hHwnd, WM_CAP_SET_PREVIEW, 1, 0);
                // 
                //  Resize window to fit in picturebox
                // 
                SetWindowPos(hHwnd, HWND_BOTTOM, 0, 0, iWidth, iHeight, (SWP_NOMOVE | SWP_NOZORDER));
            }
            else
            {
                // 
                //  Error connecting to device close window
                //  
                DestroyWindow(hHwnd);
            }

        }
        private void ClosePreviewWindow()
        {
            // 
            //  Disconnect from device
            // 
            SendMessage(hHwnd, WM_CAP_DRIVER_DISCONNECT, iDevice, 0);
            // 
            //  close window
            // 
            DestroyWindow(hHwnd);
        }
        #endregion

        public void OnVideoSend(int id, byte[] data)
        {
            if (id == int.Parse(lblNo.Text))
            {
                MemoryStream ms = new MemoryStream(data);
                pictureBox2.Image = Image.FromStream(ms);
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            this.Close();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            video.getCall(int.Parse(txtRece.Text), callType.call);

            pnlRing.Enabled = true;
            btnAnswerIn.Visible = false;
            btnCutIn.Visible = false;
            //btnStart.Enabled = false;
            //btnStop.Enabled = false;

            lblCallStatus.Text = "Calling...!";

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            SendFrame();
        }

        void SendFrame()
        {
            Console.WriteLine("send frame get call - " + lblNo.Text);
            MemoryStream ms = new MemoryStream();

            IDataObject data;
            Image bmap;

            //  Copy image to clipboard
            SendMessage(hHwnd, WM_CAP_EDIT_COPY, 0, 0);

            //  Get image from clipboard and convert it to a bitmap
            data = Clipboard.GetDataObject();

            if (data.GetDataPresent(typeof(System.Drawing.Bitmap)))
            {
                bmap = ((Image)(data.GetData(typeof(System.Drawing.Bitmap))));
                bmap.Save(ms, ImageFormat.Jpeg);
            }

            pictureBox1.Image.Save(ms, ImageFormat.Jpeg);

            byte[] buffer = ms.GetBuffer();
            Console.WriteLine("try to publish video - " + lblNo.Text);
            video.PublishVideo(int.Parse(txtRece.Text), buffer);
        }

        private void btnAnswer_Click(object sender, EventArgs e)
        {
            pnlRing.Visible = false;
            //btnStart.Enabled = false;
            //btnStop.Enabled = false;
            Console.WriteLine("start new thread - video.recieveCall - "+lblNo.Text);
            video.getCall(int.Parse(txtRece.Text), callType.incomeAnswer);
            Console.WriteLine("start new thread over" + lblNo.Text);

            //Start Video From Dial End
            iDevice = 0;
            Console.WriteLine("try to call OpenPreviewWindow - " + lblNo.Text);
            OpenPreviewWindow();
            Console.WriteLine("OpenPreviewWindow over & try to start timer - " + lblNo.Text);
            timer1.Enabled = true;
        }

        private void btnCutIn_Click(object sender, EventArgs e)
        {
            lblCallStatus.Text = "Call Ended.....!";
            video.getCall(int.Parse(txtRece.Text), callType.incomeCut);

            pnlRing.Enabled = false;
            btnCutOut.Visible = true;
            //btnStart.Enabled = true;
            //this.Close();
        }

        private void btnCutOut_Click(object sender, EventArgs e)
        {
            lblCallStatus.Text = "Call Ended.....!";
            video.getCall(int.Parse(txtRece.Text), callType.cut);

            pnlRing.Enabled = false;
            btnCutIn.Visible = true;
            btnAnswerIn.Visible = true;
            //btnStart.Enabled = true;
            //this.Close();
        }

        private void btnStartCall_Click(object sender, EventArgs e)
        {
            //Start Video Call
            iDevice = 0;
            OpenPreviewWindow();
            timer1.Enabled = true;
        }


        public void OnDial(int id, callType callType)
        {
            if (id == int.Parse(lblNo.Text) && callType == ViaService.callType.call)
            {
                pnlRing.Enabled = true;
                btnCutOut.Visible = false;
                lblCallStatus.Text = "Ringing...!";
            }
            else if (id == int.Parse(lblNo.Text) && callType == ViaService.callType.cut)
            {
                pnlRing.Enabled = false;
                btnCutOut.Visible = true;
                lblCallStatus.Text = "Call Ended....!";
            }
        }

        public void OnVideo(int id, callType callType)
        {
            if (id == int.Parse(lblNo.Text) && callType == ViaService.callType.incomeAnswer)
            {
                btnAnswerIn.Visible = true;
                btnCutIn.Visible = true;
                pnlRing.Visible = false;
                lblCallStatus.Text = "";

                //Start Call From Answer End
                iDevice = 0;
                OpenPreviewWindow();
                timer1.Enabled = true;
            }
            else if (id == int.Parse(lblNo.Text) && callType == ViaService.callType.incomeCut)
            {
                pnlRing.Enabled = false;
                btnAnswerIn.Visible = true;
                btnCutIn.Visible = true;
                lblCallStatus.Text = "Call Ended...!";
            }
        }
    }
}
