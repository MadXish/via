﻿namespace VideoClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlRing = new System.Windows.Forms.Panel();
            this.btnCutOut = new System.Windows.Forms.Button();
            this.btnCutIn = new System.Windows.Forms.Button();
            this.btnAnswerIn = new System.Windows.Forms.Button();
            this.lblCallStatus = new System.Windows.Forms.Label();
            this.pictureBoxGif = new System.Windows.Forms.PictureBox();
            this.lblNo = new System.Windows.Forms.Label();
            this.txtRece = new System.Windows.Forms.TextBox();
            this.btnStartCall = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlRing.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGif)).BeginInit();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(198, 247);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(101, 23);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Video Call";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(305, 246);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(99, 23);
            this.btnStop.TabIndex = 4;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::VideoClient.Properties.Resources._51211455;
            this.pictureBox2.Location = new System.Drawing.Point(267, 30);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(199, 172);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::VideoClient.Properties.Resources._51211455;
            this.pictureBox1.Location = new System.Drawing.Point(25, 30);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(224, 172);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // pnlRing
            // 
            this.pnlRing.Controls.Add(this.btnCutOut);
            this.pnlRing.Controls.Add(this.btnCutIn);
            this.pnlRing.Controls.Add(this.btnAnswerIn);
            this.pnlRing.Controls.Add(this.lblCallStatus);
            this.pnlRing.Controls.Add(this.pictureBoxGif);
            this.pnlRing.Location = new System.Drawing.Point(12, 12);
            this.pnlRing.Name = "pnlRing";
            this.pnlRing.Size = new System.Drawing.Size(468, 214);
            this.pnlRing.TabIndex = 9;
            // 
            // btnCutOut
            // 
            this.btnCutOut.Location = new System.Drawing.Point(26, 167);
            this.btnCutOut.Name = "btnCutOut";
            this.btnCutOut.Size = new System.Drawing.Size(75, 23);
            this.btnCutOut.TabIndex = 5;
            this.btnCutOut.Text = "Cut";
            this.btnCutOut.UseVisualStyleBackColor = true;
            this.btnCutOut.Click += new System.EventHandler(this.btnCutOut_Click);
            // 
            // btnCutIn
            // 
            this.btnCutIn.Location = new System.Drawing.Point(346, 170);
            this.btnCutIn.Name = "btnCutIn";
            this.btnCutIn.Size = new System.Drawing.Size(75, 23);
            this.btnCutIn.TabIndex = 4;
            this.btnCutIn.Text = "Cut";
            this.btnCutIn.UseVisualStyleBackColor = true;
            this.btnCutIn.Click += new System.EventHandler(this.btnCutIn_Click);
            // 
            // btnAnswerIn
            // 
            this.btnAnswerIn.Location = new System.Drawing.Point(346, 141);
            this.btnAnswerIn.Name = "btnAnswerIn";
            this.btnAnswerIn.Size = new System.Drawing.Size(75, 23);
            this.btnAnswerIn.TabIndex = 3;
            this.btnAnswerIn.Text = "Answer";
            this.btnAnswerIn.UseVisualStyleBackColor = true;
            this.btnAnswerIn.Click += new System.EventHandler(this.btnAnswer_Click);
            // 
            // lblCallStatus
            // 
            this.lblCallStatus.AutoSize = true;
            this.lblCallStatus.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCallStatus.Location = new System.Drawing.Point(190, 150);
            this.lblCallStatus.Name = "lblCallStatus";
            this.lblCallStatus.Size = new System.Drawing.Size(68, 26);
            this.lblCallStatus.TabIndex = 2;
            this.lblCallStatus.Text = "Call...!";
            // 
            // pictureBoxGif
            // 
            this.pictureBoxGif.Image = global::VideoClient.Properties.Resources.phone_ringing_gif_phone_ringing_blue_ba;
            this.pictureBoxGif.Location = new System.Drawing.Point(151, 44);
            this.pictureBoxGif.Name = "pictureBoxGif";
            this.pictureBoxGif.Size = new System.Drawing.Size(152, 93);
            this.pictureBoxGif.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxGif.TabIndex = 0;
            this.pictureBoxGif.TabStop = false;
            // 
            // lblNo
            // 
            this.lblNo.AutoSize = true;
            this.lblNo.Location = new System.Drawing.Point(33, 252);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(48, 13);
            this.lblNo.TabIndex = 10;
            this.lblNo.Text = "Acc. NO";
            // 
            // txtRece
            // 
            this.txtRece.Location = new System.Drawing.Point(410, 249);
            this.txtRece.Name = "txtRece";
            this.txtRece.Size = new System.Drawing.Size(70, 20);
            this.txtRece.TabIndex = 11;
            // 
            // btnStartCall
            // 
            this.btnStartCall.Location = new System.Drawing.Point(117, 247);
            this.btnStartCall.Name = "btnStartCall";
            this.btnStartCall.Size = new System.Drawing.Size(75, 23);
            this.btnStartCall.TabIndex = 12;
            this.btnStartCall.Text = "Start Call";
            this.btnStartCall.UseVisualStyleBackColor = true;
            this.btnStartCall.Click += new System.EventHandler(this.btnStartCall_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 279);
            this.Controls.Add(this.btnStartCall);
            this.Controls.Add(this.txtRece);
            this.Controls.Add(this.lblNo);
            this.Controls.Add(this.pnlRing);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Name = "Form1";
            this.Text = "Video";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlRing.ResumeLayout(false);
            this.pnlRing.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGif)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel pnlRing;
        private System.Windows.Forms.PictureBox pictureBoxGif;
        private System.Windows.Forms.Label lblCallStatus;
        private System.Windows.Forms.Button btnCutIn;
        private System.Windows.Forms.Button btnAnswerIn;
        private System.Windows.Forms.Label lblNo;
        private System.Windows.Forms.TextBox txtRece;
        private System.Windows.Forms.Button btnCutOut;
        private System.Windows.Forms.Button btnStartCall;
    }
}

