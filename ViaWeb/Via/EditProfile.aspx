﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditProfile.aspx.cs" Inherits="Via.EditProfile" MasterPageFile="~/Index.Master" %>

<asp:Content ID="ContentEdit" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <section style="padding-bottom: 50px; padding-top: 50px;">
            <div class="row">
                <div class="col-md-5">
                    <img src="Images/photo.jpg" class="img-rounded img-responsive" />
                    <br />
                    <a href="#" class="btn btn-success">Update Photo</a>
                    <a class="cust-nav-links formPopper" role="button" href="" id="login" data-toggle="modal" data-target="#myModal">SignIn</a>
                    <br />
                    <div class="form-group col-md-8">
                        <h3>Change Your Password</h3>
                        <br />
                        <label>Enter Old Password</label>
                        <input type="password" class="form-control">
                        <label>Enter New Password</label>
                        <input type="password" class="form-control">
                        <label>Confirm New Password</label>
                        <input type="password" class="form-control" />
                        <br>
                        <a href="#" class="btn btn-warning">Change Password</a>
                    </div>
                   
                    <br/><br/>
                </div>
                <div class="col-md-7">
                    <h3>Change Your Details</h3>
                    <label style="margin-top:44px;">First Name</label>
                    <asp:TextBox ID="txtFName" runat="server" CssClass="form-control" OnTextChanged="TextBox1_TextChanged" Width="820px"></asp:TextBox>
&nbsp;<br />
                    <label>Last Name</label>
                    <asp:TextBox ID="txtLname" runat="server" CssClass="form-control" OnTextChanged="TextBox1_TextChanged" Width="820px"></asp:TextBox>
&nbsp;<label>Email</label>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" OnTextChanged="TextBox1_TextChanged" Width="820px"></asp:TextBox>
&nbsp;<label>Address</label>
                    <asp:TextBox ID="txtAdd" runat="server" CssClass="form-control" OnTextChanged="TextBox1_TextChanged" Width="820px"></asp:TextBox>
&nbsp;<label>DOB</label>
                    <asp:TextBox ID="txtDOB" runat="server" CssClass="form-control" OnTextChanged="TextBox1_TextChanged" Width="820px"></asp:TextBox>
&nbsp;<label>Tel</label>
                    <asp:TextBox ID="txtxTel" runat="server" CssClass="form-control" OnTextChanged="TextBox1_TextChanged" Width="820px"></asp:TextBox>
&nbsp;<label>Gender</label><br />
                    
                    <br /><br />
                    <a href="#" class="btn btn-success">Update Details</a>
                    <asp:ListBox ID="ListBox1" runat="server" CssClass="form-control"></asp:ListBox>
                    
                </div>
            </div>
            <!-- ROW END -->


        </section>
        <!-- SECTION END -->
    </div>
</asp:Content>

