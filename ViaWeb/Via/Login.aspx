﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Via.Login" MasterPageFile="~/Index.Master" %>


    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="container">
        <%--<div class="auto-style1">--%>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
            <div id="banner">
                <h1>
                    Via <strong>Video</strong> Chat</h1>
                <h5>
                    <strong style="color:black">www.viaweb.com</strong></h5>
            </div>
        </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="registrationform" style="margin-top:20px;">
                <div class="form-horizontal">
        <h1>Login </h1>
        <table class="auto-style2">
            <tr>
                <td class="auto-style6">User Name</td>
                <td class="auto-style9">
                    <asp:TextBox ID="txtUserName" runat="server" Width="241px" CssClass="form-control"></asp:TextBox>
                </td>
                <td class="auto-style8">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserName" ErrorMessage="Please Enter Username" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr">
                <td class="auto-style10">Password</td>
                <td class="auto-style5">
                    <asp:TextBox ID="TxtPassword" runat="server" TextMode="Password" Width="242px" CssClass="form-control"></asp:TextBox>
                </td>
                <td class="auto-style7">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TxtPassword" ErrorMessage="Please enter Password" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style11"></td>
                <td class="auto-style12">
                    <asp:Label ID="lblStatus" runat="server" ForeColor="Red"></asp:Label>
                </td>
                <td class="auto-style4"></td>
            </tr>
            <tr>
                <td class="auto-style10">&nbsp;</td>
                <td class="auto-style5">
                    <asp:Button ID="btnLogin" CssClass="btn btn-success" runat="server" OnClick="btnLogin_Click" Text="Login" />
                &nbsp;&nbsp;&nbsp;
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Registration.aspx"><b>New User...?</b></asp:HyperLink>
                </td>
                <td class="auto-style7">
                    &nbsp;</td>
            </tr>
        </table>
    </div>
                </div>
    </div>
        </div>
            <%--</div>--%>
            </div>
 </asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .auto-style1 {
            width: 1082px;
        }

        .auto-style2 {
            width: 532px;
        }

        .auto-style4 {
            width: 88px;
            height: 27px;
        }

        .auto-style5 {
            height: 49px;
            width: 171px;
        }

        .auto-style6 {
            width: 77px;
            height: 72px;
        }

        .auto-style7 {
            height: 50px;
        }

        .auto-style8 {
            height: 72px;
        }

        .auto-style9 {
            height: 72px;
            width: 171px;
        }

        .auto-style10 {
            width: 77px;
        }

        .auto-style11 {
            width: 77px;
            height: 27px;
        }

        .auto-style12 {
            height: 27px;
            width: 171px;
        }
    </style>
</asp:Content>


