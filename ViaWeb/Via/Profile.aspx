﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="Via.Profile" MasterPageFile="~/Index.Master" %>

<asp:Content ID="ContentProfile" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row">
            <h1>My Account</h1>
        </div>
        <div class="row" style="background-color:rgb(231,241,245); margin-top:20px;">
            <div class="col-md-3" style="background-color:rgb(0,174,239); padding-bottom:20px;">
                <div class="row" style="height:250px;">
                <img src="Images/user3.jpg" style="height:250px; width:100%; background-repeat:no-repeat; background-size:cover"/>
                </div>
            <aside style="margin-top:20px;">
        	
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="Profile.aspx">Prifile</a></li>
                <li><a href="#">Edit Profile</a></li>
                <li><a href="Contacts.aspx">Contacts</a></li>
                <li><a href="#">Help</a></li>
            </ul> 
         </aside>
            </div>
            <div class="col-md-9" style="background-color:white; padding:10px;">

                <div class="row">
                    <div class="col-md-3 profil-pic" id="profilePic">
                        <img src="Images/contacts.png" style="height:140px;" />
                    </div>
                    <div class="col-md-9">
                        <h2>Via Friends</h2>
                        <p>You Can Manage Your Friends In Your Contact List.
                            You can Add Friends
                          You Can Remove Friends
                           And grupe Chat
                        </p>
                    </div>
                </div>

                  <div class="row">
               
                <table class="auto-style1 table table-condensed" style="margin-left:20px; margin-top:30px;">
                    <tr>
                        <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">First Name</td>
                        <td class="active" style="width: 497px">
                            <asp:Label ID="lblFName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">Last Name</td>
                        <td class="active" style="width: 497px">
                            <asp:Label ID="lblLName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">Email</td>
                        <td class="active" style="width: 497px">
                            <asp:Label ID="lblEmail" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">Address</td>
                        <td class="active" style="width: 497px">
                            <asp:Label ID="lblAddress" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">DOB</td>
                        <td class="active" style="width: 497px">
                            <asp:Label ID="lblDOB" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">Tel</td>
                        <td class="active" style="width: 497px">
                            <asp:Label ID="lblTel" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">Gender</td>
                        <td class="active" style="width: 497px">
                            <asp:Label ID="lblGender" runat="server"></asp:Label>
                        </td>
                    </tr>
                    
                </table>
&nbsp;&nbsp;&nbsp;&nbsp; <asp:Button ID="btnProfLogout" CssClass="btn btn-success" runat="server" OnClick="Button1_Click" Text="LogOut" />
          </div>
    </div>
 </div>
 </div>   

    </asp:Content>

<asp:Content ID="Content1" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .auto-style1 {
            width: 92%;
        }
        .auto-style2{
            height:36px;
        }
        </style>
   
</asp:Content>

