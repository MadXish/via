﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Via.Classes
{
    public class Hash
    {
        private string salt = "texyChat48][;'9023jHFU";

        public Hash()
        {

        }
        public Hash(string salt)
        {
            this.salt = salt;
        }

        public string GenerateSHA256Hash(string pass)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(pass + this.salt);
            System.Security.Cryptography.SHA256Managed sha256hashstring = new System.Security.Cryptography.SHA256Managed();
            byte[] hash = sha256hashstring.ComputeHash(bytes);
            return this.ByteArrayToString(hash);
        }

        private string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }
    }
}