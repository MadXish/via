﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImageTest.aspx.cs" Inherits="Via.ImageTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="GridView1" runat="server">
           <Columns>
               <asp:TemplateField HeaderText="Image">
                   <ItemTemplate>
                       <asp:Image ID="img" runat="server" ImageUrl="<%#Eval("Image") %>" Height="100" Width="100" />
                   </ItemTemplate>
               </asp:TemplateField>
           </Columns>
        </asp:GridView>
    </div>
    </form>
</body>
</html>
