﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Via
{
    public partial class EditProfile : System.Web.UI.Page
    {
        string strConnString = ConfigurationManager.ConnectionStrings["RegistrationConnectionString"].ConnectionString;
        string str;
        SqlDataAdapter sqlda;
        DataSet ds;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindgrid();
            }
        }

        private void bindgrid()
        {
            string UserName = (string)Session["F_Name"];
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            str = "SELECT * FROM Userdata WHERE F_Name='"+UserName+"'";
            sqlda = new SqlDataAdapter(str, con);
            ds = new DataSet();
            sqlda.Fill(ds);
            if (ds != null)
            {
                txtFName.Text = ds.Tables["Userdata"].Rows[0]["F_Name"].ToString();
                txtLname.Text = ds.Tables["Userdata"].Rows[0]["L_Name"].ToString();
                txtEmail.Text = ds.Tables["Userdata"].Rows[0]["Email"].ToString();
                txtAdd.Text = ds.Tables["Userdata"].Rows[0]["Address"].ToString();
                txtDOB.Text = ds.Tables["Userdata"].Rows[0]["DOB"].ToString();
                txtxTel.Text = ds.Tables["Userdata"].Rows[0]["Tel"].ToString();
                ListBox1.Text = ds.Tables["Userdata"].Rows[0]["Gender"].ToString();
            }
            con.Close();
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}